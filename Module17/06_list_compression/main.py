import random

def zero_to_end(list):
    for _ in range(list.count(0)):
        list.remove(0)
        list.append(0)

def remove_zero(list):
    for _ in range(list.count(0)):
        list.remove(0)

N = random.randint(10, 20)
list = [random.randint(-2, 2) for _ in range(N)]

print(list)
zero_to_end(list)
print(list)
remove_zero(list)
print(list)


