vowels_list = ['а', 'о', 'и', 'е', 'ё', 'э', 'ы', 'у', 'ю', 'я']
text = list(input('Введите текст: '))

vowels_in_text = [letter for letter in text if letter in vowels_list]
print('Список гласных букв: ', vowels_in_text)
print('Длина списка:', len(vowels_in_text))
