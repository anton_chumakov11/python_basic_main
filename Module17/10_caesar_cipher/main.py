alphabit = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й',
            'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
            'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']

string = list(input('Введите сообщение (не используйте заглавные буквы!!): '))
k = int(input('Введите сдвиг: '))
if k > len(alphabit):
    k %= len(alphabit)

for i in range(len(string)):
    if string[i] in alphabit:
        index = alphabit.index(string[i])
        if index + k < len(alphabit):
            index += k
        else:
            index = index - len(alphabit) + k
        string[i] = alphabit[index]
print(''.join(string))







