import random

N = random.randint(1, 20)
print('Кол-во палок:', N)
# list_of_wand = list(range(1, N + 1))
list_of_wand = ['|' for _ in range(N)]

K = random.randint(0, N // 2)
print('Кол-во бросков:', K)

for i in range(1, K + 1):
    L_i = random.randint(1, N // 2)
    R_i = random.randint(L_i + 1, N)
    count = R_i - L_i + 1
    print('Бросок', str(i) + '. Сбиты палки с номера', L_i, 'по номер', R_i)
    list_of_wand[L_i - 1:R_i] = ['.' for _ in range(count)]

print('\nРезультат:', ''.join(list_of_wand))


