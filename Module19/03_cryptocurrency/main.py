def print_keys(data):
    print('Список ключей:')
    keys_set = set()
    for i_elem in data.keys():
        keys_set.add(i_elem)
        if isinstance(data[i_elem], dict):
            for j_elem in data[i_elem].keys():
                keys_set.add(j_elem)
        elif isinstance(data[i_elem], list):
            for j_elem in data[i_elem]:
                for k_elem in j_elem.keys():
                    keys_set.add(k_elem)
                    if isinstance(j_elem[k_elem], dict):
                        for n_elem in j_elem[k_elem].keys():
                            keys_set.add(n_elem)
    print(keys_set)

def print_values(data):
    print('Список значений:')
    values_set = set()
    for i_elem in data.keys():
        if isinstance(data[i_elem], dict):
            for j_elem in data[i_elem].values():
                values_set.add(j_elem)
        elif isinstance(data[i_elem], list):
            for j_elem in data[i_elem]:
                if isinstance(j_elem, dict):
                    for k_elem in j_elem.values():
                        if isinstance(k_elem, dict):
                            for n_elem in k_elem.values():
                                values_set.add(n_elem)
                        else:
                           values_set.add(k_elem)
                else:
                    values_set.add(data[j_elem])
        else:
            values_set.add(data[i_elem])
    print(values_set)


data = {
    "address": "0x544444444444",
    "ETH": {
        "balance": 444,
        "total_in": 444,
        "total_out": 4
    },
    "count_txs": 2,
    "tokens": [
        {
            "fst_token_info": {
                "address": "0x44444",
                "name": "fdf",
                "decimals": 0,
                "symbol": "dsfdsf",
                "total_supply": "3228562189",
                "owner": "0x44444",
                "last_updated": 1519022607901,
                "issuances_count": 0,
                "holders_count": 137528,
                "price": False
            },
            "balance": 5000,
            "totalIn": 0,
            "total_out": 0
        },
        {
            "sec_token_info": {
                "address": "0x44444",
                "name": "ggg",
                "decimals": "2",
                "symbol": "fff",
                "total_supply": "250000000000",
                "owner": "0x44444",
                "last_updated": 1520452201,
                "issuances_count": 0,
                "holders_count": 20707,
                "price": False
            },
            "balance": 500,
            "totalIn": 0,
            "total_out": 0
        }
    ]
}

print_keys(data)
print_values(data)
data["ETH"]["total_diff"] = 100
data["tokens"][0]["fst_token_info"]["name"] = "doge"
data["ETH"]["total_out"] = data["tokens"][0]["total_out"]
del data["tokens"][0]["total_out"]
data["tokens"][1]["sec_token_info"]["total_price"] = data["tokens"][1]["sec_token_info"].pop("price")


