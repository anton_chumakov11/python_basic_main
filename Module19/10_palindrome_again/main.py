def polindrom(string):
    symbol_dict = {}
    for sym in string:
        symbol_dict[sym] = symbol_dict.get(sym, 0) + 1
    count = 0
    for value in symbol_dict.values():
        if value % 2 != 0:
            count += 1
    return count <= 1

string = input('Введите строку: ')

if polindrom(string):
    print('Можно сделать палиндромом')
else:
    print('Нельзя сделать палиндромом')
