def filling_dict(number):
    cities_dict = dict()
    for i in range(1, number + 1):
        print(i, 'страна:', end = ' ')
        country_and_cities = input().split(' ')
        cities_dict[country_and_cities[0]] = set(country_and_cities[1:])
    return cities_dict

def finding_cities(dict):
    for i in range(1, 4):
        print('\n', i, 'город:', end = ' ')
        city = input()
        isExist = False
        for country in dict.keys():
            if city in dict[country]:
                print('Город {0} расположен в стране {1}.'.format(city, country))
                isExist = True
        if not isExist:
            print('По городу {} данных нет'.format(city))



number_of_countries = int(input('Кол-во стран: '))
cities_dict = filling_dict(number_of_countries)
finding_cities(cities_dict)


