def sum_and_cost(dict_product, dict_count):
    for product_id in dict_product.values():
        count = 0
        price = 0
        for store in dict_count[product_id]:
            count += store['quantity']
            price += (store['price'] * store['quantity'])
        for product_name in dict_product.keys():
            if dict_product[product_name] == product_id:
                this_product_name = product_name
        print('{0} - {1} шт, стоимость {2} руб'.format(this_product_name, count, price))



goods = {
    'Лампа': '12345',
    'Стол': '23456',
    'Диван': '34567',
    'Стул': '45678',
}

store = {
    '12345': [
        {'quantity': 27, 'price': 42},
    ],
    '23456': [
        {'quantity': 22, 'price': 510},
        {'quantity': 32, 'price': 520},
    ],
    '34567': [
        {'quantity': 2, 'price': 1200},
        {'quantity': 1, 'price': 1150},
    ],
    '45678': [
        {'quantity': 50, 'price': 100},
        {'quantity': 12, 'price': 95},
        {'quantity': 43, 'price': 97},
    ],
}

sum_and_cost(goods, store)
