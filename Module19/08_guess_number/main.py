def game(n):
    set_full = set(str(i) for i in range (1, n + 1))
    phrase = input('Нужное число есть среди вот этих чисел: ')
    set_nums = set(phrase.split(' '))
    while phrase != 'Помогите!':
        answer = input('Ответ Артёма: ')
        if answer == 'Да':
            set_full.intersection_update(set_nums)
        else:
            set_full -= set_nums
        phrase = input('Нужное число есть среди вот этих чисел: ')
        set_nums = set(phrase.split(' '))
    print('Артём мог загадать следующие числа:', end = ' ')
    for num in set_full:
        print(num, end = ' ')


n = int(input('Введите максимальное число: '))
game(n)
