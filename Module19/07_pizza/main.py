def filling_dict(number):
    order_dict = dict()
    for i in range(1, number + 1):
        print(i, 'заказ:', end = ' ')
        order = input().split(' ')
        if order[0] in order_dict:
            if order[1] in order_dict[order[0]]:
                order_dict[order[0]][order[1]] += int(order[2])
            else:
                order_dict[order[0]][order[1]] = int(order[2])
        else:
            order_dict[order[0]] = dict()
            order_dict[order[0]][order[1]] = int(order[2])
    return order_dict

def print_dict(dict):
    for key in dict.keys():
        print('{}:'.format(key))
        for key1 in dict[key].keys():
            print('\t{0}: {1}'.format(key1, dict[key][key1]))

number = int(input('Введите кол-во заказов: '))
order_dict = filling_dict(number)
print_dict(order_dict)
