def filling_dict(number):
    couple_dict = dict()
    for i in range(1, number_of_couple + 1):
        print(i, 'пара:', end = ' ')
        couple_list = input().lower().split(' - ')
        couple_dict[couple_list[0]] = couple_list[1]
    return couple_dict

def find_synonym(dict):
    word = input('Введите слово: ').lower()
    while not word in dict.keys() and not word in dict.values():
        word = input('Введите слово: ').lower()
    if word in dict.keys():
        print('Синоним:', dict[word])
    else:
        for key in dict.keys():
            if dict[key] == word:
                print('Синоним:', key)

number_of_couple = int(input('Введите количество пар слов: '))
couple_dict = filling_dict(number_of_couple)
find_synonym(couple_dict)
