def frequency_dict(string):
    symbol_dict = dict()
    for symbol in string:
        if symbol in symbol_dict:
            symbol_dict[symbol] += 1
        else:
            symbol_dict[symbol] = 1
    return symbol_dict

def print_dict(dict):
    for letter in sorted(dict.keys()):
        print('{0} : {1}'.format(letter, dict[letter]))

def invert_frequency_dict(diction):
    invert_dict = dict()
    for letters in diction.keys():
        if diction[letters] in invert_dict:
            invert_dict[diction[letters]].append(letters)
        else:
            invert_dict[diction[letters]] = []
            invert_dict[diction[letters]].append(letters)
    return invert_dict


text = input('Введите текст: ')
original_frequency_dict = frequency_dict(text)
print('Оригинальный словарь частот:')
print_dict(original_frequency_dict)
invert_dict = invert_frequency_dict(original_frequency_dict)
print('\nИнвертированный словарь частот:')
print_dict(invert_dict)

