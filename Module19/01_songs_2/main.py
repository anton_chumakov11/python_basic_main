def filling_set(number):
    song_set = set()
    for i in range(1, number + 1):
        print('Название {0} песни:'.format(i), end = ' ')
        user_song = input()
        song_set.add(user_song)
    return song_set

violator_songs = {
    'World in My Eyes': 4.86,
    'Sweetest Perfection': 4.43,
    'Personal Jesus': 4.56,
    'Halo': 4.9,
    'Waiting for the Night': 6.07,
    'Enjoy the Silence': 4.20,
    'Policy of Truth': 4.76,
    'Blue Dress': 4.29,
    'Clean': 5.83
}

number_of_songs = int(input('Сколько песен выбрать? '))
users_songs = filling_set(number_of_songs)
songs_length = 0

for song in violator_songs.keys():
    if song in users_songs:
        songs_length += violator_songs.get(song)

print('Общее время звучания песен: {:.2f}'.format(songs_length))

