def filling_family_dict(number):
    family_dict = dict()
    for i in range(1, number):
        print(i, 'пара:', end = ' ')
        couple = input().split(' ')
        if couple[1] in family_dict:
            family_dict[couple[1]].append(couple[0])
        else:
            family_dict[couple[1]] = []
            family_dict[couple[1]].append(couple[0])
    return family_dict

def filling_set_family_member(family_dict):
    set_family_member = set()
    for key in family_dict.keys():
        set_family_member.add(key)
    for child_list in family_dict.values():
        for child in child_list:
            set_family_member.add(child)
    return set_family_member

def set_keys(dict):
    set_keys = set()
    for key in dict.keys():
        set_keys.add(key)
    return set_keys

def set_values(dict):
    set_values = set()
    for child_list in dict.values():
        for child in child_list:
            set_values.add(child)
    return set_values

def find_key(dict, value):
    for key in dict.keys():
        for element in dict[key]:
            if element == value:
                return key


def delete_value_from_dict(dict, value):
    key = find_key(dict, value)
    for name_list in dict.values():
        for name in name_list:
            if name == value:
                name_list.remove(name)
    if dict[key] == []:
        dict.pop(key)


def filling_high_dict(family_dict):
    high_dict = dict()
    # set_family_member = list(filling_set_family_member(family_dict))
    keys = set_keys(family_dict)
    values = set_values(family_dict)
    keys.difference_update(values)
    zero_person = list(keys)
    while any(family_dict):
        high = 0
        tmp_person = zero_person[0]
        while tmp_person in family_dict:
            tmp_person = family_dict[tmp_person][len(family_dict[tmp_person]) - 1]
            high += 1
        high_dict[tmp_person] = high
        delete_value_from_dict(family_dict, tmp_person)
    high_dict[zero_person[0]] = 0
    # set_family_member = set(set_family_member)
    # set_child = set(high_dict.keys())
    # set_family_member.difference_update(set_child)
    # for i in set_family_member:
    #     high_dict[i] = 0
    return high_dict

def print_dict(dict):
    list_keys = list(dict.keys())
    list_keys.sort()
    print('\n“Высота” каждого члена семьи')
    for key in list_keys:
        print(key, dict[key])

number_of_person = int(input('Введите количество человек: '))
family_dict = filling_family_dict(number_of_person)
high_dict = filling_high_dict(family_dict)
print_dict(high_dict)

