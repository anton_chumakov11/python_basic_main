import math

def distance(x,y):
    dist = math.sqrt(x ** 2 + y ** 2)
    return dist

print('Введите координаты монетки:')
x = float(input('X: '))
y = float(input('Y: '))
r = float(input('Введите радиус: '))

if distance(x, y) <= r:
    print('Монетка где-то рядом')
else:
    print('Монетки в области нет')

