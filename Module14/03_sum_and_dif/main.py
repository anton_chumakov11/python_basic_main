def count_of_digits(number):
    count = 0
    while number > 0:
        number //= 10
        count += 1
    return count

def sum_of_digits(number):
    summ = 0
    while number >0:
        summ += number % 10
        number //= 10
    return summ

n = int(input('Введите число: '))

print('Сумма цифр:', sum_of_digits(n))
print('Кол-во цифр в числе:', count_of_digits(n))
print('Разность суммы и кол-ва цифр:', sum_of_digits(n) - count_of_digits(n))

