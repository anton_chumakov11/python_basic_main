def min_divider(n):
    divider = 0
    for i in range(2, n + 1):
        if n % i == 0:
            divider = i
            break
    return divider

N = int(input('Введите число: '))
print('Наименьший делитель, отличный от единицы:', min_divider(N))
