def same_digit_count(year):
    str_year = str(year)
    count = 0
    max_count = 0
    while year > 0:
        a = year % 10
        for i in str_year:
            if str(a) == i:
                count += 1
        if count > max_count:
            max_count = count
        year //= 10
        count = 0
    return max_count

year1 = int(input('Введите первый год: '))
year2 = int(input('Введите второй год: '))

print('Годы от', year1, 'до', year2, 'с тремя одинаковыми цифрами:')
for i in range(year1, year2 + 1):
    if same_digit_count(i) == 3:
        print(i)


