def calculating_math_func(data, dct={}):
    result = 1
    if data in dct:
        result = dct[data]
    else:
        for index in range(1, data + 1):
            result *= index
        dct[data] = result
    result /= data ** 3
    result = result ** 10
    return result

res = calculating_math_func(10)
res1 = calculating_math_func(10)
print(res)
