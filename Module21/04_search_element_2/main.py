def find(dct, key, default_depth=False):
	if not default_depth:
		default_depth = 1000000
	current_depth = 0
	if key in dct:
		return dct[key]
	if current_depth < default_depth:
		current_depth += 1
		for value in dct.values():
			if isinstance(value, dict):
				result = find(value, key, default_depth-1)
				if result:
					break
		else:
			return None
	else:
		return None
	return result

site = {
	'html': {
		'head': {
			'title': 'Мой сайт'
		},
		'body': {
			'h2': 'Здесь будет мой заголовок',
			'div': 'Тут, наверное, какой-то блок',
			'p': 'А вот здесь новый абзац'
		}
	}
}

user_key = input('Искомый ключ: ')
is_user_depth = int(input('Хотите ввести глубину поиска? 1 - да, 2 - нет: '))
if is_user_depth == 1:
	depth = int(input('Желаемая глубина: '))
	result = find(site, user_key, depth)
else:
	result = find(site, user_key)
if result:
	print('Значение:', result)
else:
	print('Такого ключа в структуре сайта нет.')
