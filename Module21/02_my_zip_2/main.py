def my_zip(elem1, elem2):
    elem1 = list(elem1)
    elem2 = list(elem2)
    return [(elem1[i], elem2[i]) for i in range(min(len(elem1), len(elem2)))]

mltpl = (1, 2, 3)
my_tuple = 10, 20, 30, 40
dct = {1: 2, 3: 4}
string = 'asdasd'
lst = [11, 12, 13, 14, 15]

generator = my_zip(string, my_tuple)
generator2 = my_zip(dct, mltpl)
generator3 = my_zip(lst, dct)
print(generator)
print(generator2)
print(generator3)




