def create_new_site(site_template, product_name):
    new_site = copy_site(site_template)
    string = 'Куплю/продам ' + product_name + ' недорого'
    new_site['html']['head']['title'] = string
    string = 'У нас самая низкая цена на ' + product_name
    new_site['html']['body']['h2'] = string
    return new_site

def copy_site(dct):
    new_dct = dict()
    for i_key, i_elem in dct.items():
        if isinstance(i_elem, dict):
            new_dct[i_key] = copy_site(i_elem)
        else:
            new_dct[i_key] = i_elem
    return new_dct

def create_sites(site_template, num_of_sites):
    dct_of_sites = dict()
    for _ in range(num_of_sites):
        product_name = input('Введите название продукта для нового сайта: ')
        dct_of_sites[product_name] = create_new_site(site_template, product_name)
    return dct_of_sites

def print_dct(dct, gap=0):
    for i_key, i_value in dct.items():
        print('\n' + '\t' * gap + str(i_key) + ':', end = ' ')
        if isinstance(i_value, dict):
            print_dct(i_value, gap+1)
        else:
            print(i_value, end = ' ')

def printing_sites(dct_of_sites):
   for i_key, i_value in dct_of_sites.items():
       print('\nСайт для ' + str(i_key) + ':')
       print_dct(i_value)
       print()

site = {
    'html': {
        'head': {
            'title': 'Куплю/продам телефон недорого'
        },
        'body': {
            'h2': 'У нас самая низкая цена на iphone',
            'div': 'Купить',
            'p': 'продать'
        }
    }
}

num_of_sites = int(input('Сколько сайтов: '))
sites = create_sites(site, num_of_sites)
printing_sites(sites)

