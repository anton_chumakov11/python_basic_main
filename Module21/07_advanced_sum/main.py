# def expanding(lst, new_list=[]):
#     if isinstance(lst, list):
#         for i_elem in lst:
#             if isinstance(i_elem, list):
#                 expanding(i_elem)
#             else:
#                 new_list.append(i_elem)
#     else:
#         new_list.append(lst)
#     return new_list
#
# def my_sum(*args):
#     summ = 0
#     for i_elem in args:
#         elem = expanding(i_elem)
#     summ += sum(elem)
#     print('Ответ:', summ)

def my_sum(*args):
    summ = 0
    for i_elem in args:
        if isinstance(i_elem, int):
            summ += i_elem
        elif isinstance(i_elem, (list, tuple)):
            for j_elem in i_elem:
                summ += my_sum(j_elem)
    return summ


print(my_sum([[1, 2, [3]], [1], 3]))
print(my_sum(1, 2, 3, 4, 5))
print(my_sum([1, [2]], [3, 4]))
