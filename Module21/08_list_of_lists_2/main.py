def expanding(lst, new_list=[]):
    for i_elem in lst:
        if isinstance(i_elem, list):
            expanding(i_elem)
        else:
            new_list.append(i_elem)
    return new_list


nice_list = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10]],
             [[11, 12, 13], [14, 15], [16, 17, 18]]]

lst = expanding(nice_list)
print(lst)


