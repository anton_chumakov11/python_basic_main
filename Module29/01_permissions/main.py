from typing import Callable
import functools

user_permissions = ['admin']


def check_permission(user: str = None) -> Callable:
    def check(func) -> Callable:
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            try:
                if user in user_permissions:
                    return func(*args, **kwargs)
                else:
                    raise PermissionError
            except PermissionError:
                print('У пользователя {} недостаточно прав, чтобы выполнить функцию {}'.format(user, func.__name__))
        return wrapped
    return check


@check_permission('admin')
def delete_site():
    print('Удаляем сайт')


@check_permission('user_1')
def add_comment():
    print('Добавляем комментарий')


delete_site()
add_comment()
