import datetime
import time
import functools


def format_time(date, f):
    sym = 'bBdDyYhHmMSs'
    f2 = ''
    for i_sym in f:
        if i_sym in sym:
            f2 += '%'
        f2 += i_sym
    return date.strftime(f2)


def timer(func, clas, form):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        f = form
        start = datetime.datetime.now()
        print('-Запускается {}.{}. Дата и время запуска: {}'.format(clas.__name__, func.__name__, format_time(start, f)))
        result = func(*args, **kwargs)
        end = datetime.datetime.now()
        period = end - start
        print('-Завершение {}.{}. Время работы = {}'.format(clas.__name__, func.__name__, period))
        return result
    return wrapper


def log_methods(form):
    # @functools.wraps()
    def decorate(cls):
        for i_method in dir(cls):
            if i_method.startswith('__') is False:
                cur_met = getattr(cls, i_method)
                decorated_method = timer(cur_met, cls, form)
                setattr(cls, i_method, decorated_method)
        return cls
    return decorate


@log_methods("b d Y - H:M:S")
class A:
    def test_sum_1(self) -> int:
        print('test sum 1')
        number = 100
        result = 0
        for _ in range(number + 1):
            result += sum([i_num ** 2 for i_num in range(10000)])
        return result


@log_methods("b d Y - H:M:S")
class B(A):
    def test_sum_1(self):
        super().test_sum_1()
        print("Наследник test sum 1")

    def test_sum_2(self):
        print("test sum 2")
        number = 200
        result = 0
        for _ in range(number + 1):
            result += sum([i_num ** 2 for i_num in range(10000)])
        return result


my_obj = B()
my_obj.test_sum_1()
my_obj.test_sum_2()
