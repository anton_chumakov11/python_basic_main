def summ_symbol(file_name):
    summ = 0
    line_number = 0
    with open(file_name, 'r', encoding='utf-8') as input_file:
        for i_line in input_file:
            try:
                line_number += 1
                length = len(i_line)
                if i_line.endswith('\n'):
                    length -= 1
                if length < 3:
                    raise BaseException('В {} строке меньше 3 символов!'.format(line_number))
                summ += length
            except BaseException:
                print('В {} строке меньше 3 символов!'.format(line_number))
    return summ

file_name = 'people.txt'
summ_char = summ_symbol(file_name)
print(summ_char)
