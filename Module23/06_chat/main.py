def chat(user_name):
    what_to_do = int(input('Выберите действие:\n'
                           '    1. Посмотреть текущий текст чата.\n'
                           '    2. Отправить сообщение.\n:'))
    while what_to_do not in (1, 2):
        what_to_do = int(input('Некорректное действие!\nВыберите действие повторно:\n'
                           '    1. Посмотреть текущий текст чата.\n'
                           '    2. Отправить сообщение.\n:'))
    if what_to_do == 1:
        with open('chat.txt', 'r', encoding='utf-8') as chat_file:
            chat = chat_file.read()
            print(chat)
    elif what_to_do == 2:
        message = input('Введите сообщение: ')
        with open('chat.txt', 'a', encoding='utf-8') as chat_file_wright:
            string = user_name + ': ' + message + '\n'
            chat_file_wright.write(string)



while True:
    user_name = input('Введите имя пользователя: ')
    chat(user_name)
    print()
