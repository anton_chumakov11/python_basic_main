def validation_line(string):
    try:
        lst = string.split(' ')
        operation = '+-*//%'
        if len(lst) != 3:
            raise IndexError
        if lst[1] not in operation:
            raise SyntaxError
        if not lst[0].isdigit() and not lst[2].isdigit():
            raise ValueError
    except (IndexError, SyntaxError, ValueError):
        return False
    return True

def calc(string):
    lst = string.split(' ')
    operand1 = int(lst[0])
    operand2 = int(lst[2])
    if lst[1] == '+':
        result = operand1 + operand2
    elif lst[1] == '-':
        result = operand1 - operand2
    elif lst[1] == '*':
        result = operand1 * operand2
    elif lst[1] == '//':
        result = operand1 // operand2
    elif lst[1] == '/':
        result = operand1 / operand2
    elif lst[1] == '%':
        result = operand1 % operand2
    print(string[:-1], '=', result)
    return result

def calculate(file_name):
    summ = 0
    with open(file_name, 'r', encoding='utf-8') as input_file:
        for i_line in input_file:
            if validation_line(i_line):
                result = calc(i_line)
                summ += result
    print('Сумма результатов вычислений:', summ)

file_name = 'calc.txt'
calculate(file_name)
