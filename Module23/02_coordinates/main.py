import random


def f(x, y):
    try:
        x += random.randint(0, 10)
        y += random.randint(0, 5)
        result = x / y
    except ZeroDivisionError:
        result = 0
    return result


def f2(x, y):
    try:
        x -= random.randint(0, 10)
        y -= random.randint(0, 5)
        result = y / x
    except ZeroDivisionError:
        result = 0
    return result


try:
    with open('coordinates.txt', 'r') as file:
        for line in file:
            nums_list = line.split()
            res1 = f(int(nums_list[0]), int(nums_list[1]))
            res2 = f2(int(nums_list[0]), int(nums_list[1]))
            number = random.randint(0, 100)
            with open('result.txt', 'a') as file_2:
                my_list = sorted([res1, res2, number])
                file_2.write(' '.join(str(i_elem) for i_elem in my_list) + '\n')
except FileNotFoundError:
    print("Файл coordinates.txt не найден")

