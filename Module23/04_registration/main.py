def validation_line(string):
    try:
        lst = string.split(' ')
        if len(lst) != 3:
            raise IndexError
        if not lst[0].isalpha():
            raise NameError
        if ('@' not in lst[1]) and ('.' not in lst[1]):
            raise SyntaxError
        if (not lst[2].isdigit()) and (int(lst[2]) > 99 or  int(lst[2]) < 0):
            raise ValueError
    except IndexError:
        return False, 'IndexError\n'
    except NameError:
        return False, 'NameError\n'
    except SyntaxError:
        return False, 'SyntaxError\n'
    except ValueError:
        return False, 'ValueError\n'
    return True, None

def validation(file_name):
    with open(file_name, 'r', encoding='utf-8') as input_file:
        for i_line in input_file:
            is_good, error = validation_line(i_line)
            if is_good:
                with open('registrations_good.log', 'a', encoding='utf-8') as good_log:
                    good_log.write(i_line)
            else:
                with open('registrations_bad.log', 'a', encoding='utf-8') as bad_log:
                    string = i_line[:-1] + ' ' + error
                    bad_log.write(string)

file_name = 'registrations.txt'
validation(file_name)
