import io


class File:
    def __init__(self, file_name, mode='w'):
        self.file_name = file_name
        self.mode = mode

    def __enter__(self):
        try:
            self.file = open(self.file_name, self.mode, encoding='UTF-8')
        except FileNotFoundError:
            self.file = open(self.file_name, 'w', encoding='UTF-8')
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()
        return True


with File('example.txt', 'r') as file:
    file.write('Всем привет!')
