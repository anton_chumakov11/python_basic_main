from math import sqrt


class Square:
    def __init__(self, edge):
        self.__edge = edge

    def __str__(self):
        return 'Квадрат со стороной {}'.format(self.__edge)

    @property
    def edge(self):
        return self.__edge

    @edge.setter
    def edge(self, edge):
        if edge <= 0:
            raise ValueError('Недопустимая длина ребра квадрата')
        else:
            self.__edge = edge

    def square_area(self):
        return self.__edge ** 2

    def square_perimeter(self):
        return self.__edge * 4


class Cube:
    def __init__(self, edge):
        self.__square_lst = [Square(edge) for _ in range(8)]

    def __str__(self):
        return 'Данный куб состоит из {} квадратов со стороной {}'.format(len(self.__square_lst), self.__square_lst[0].edge)

    @property
    def edge(self):
        return self.__square_lst[0].edge

    @edge.setter
    def edge(self, edge):
        if edge <= 0:
            raise ValueError('Недопустимая длина ребра куба')
        else:
            self.__square_lst = [Square(edge) for _ in range(8)]

    def cube_surface(self):
        s = self.__square_lst[0].square_area() * len(self.__square_lst)
        return s


class Triangle:
    def __init__(self, base, heigh):
        self.__base = base
        self.__heigh = heigh

    def __str__(self):
        return 'Треугольник с высотой {} и основанием {}'.format(self.__base, self.__heigh)

    @property
    def base(self):
        return self.__base

    @base.setter
    def base(self, base):
        if base <= 0:
            raise ValueError('Некорректное значение высоты')
        else:
            self.__base = base

    @property
    def heigh(self):
        return self.__heigh

    @base.setter
    def base(self, heigh):
        if hegh <= 0:
            raise ValueError('Некорректное значение высоты')
        else:
            self.__heigh = heigh

    def triangle_area(self):
        return (self.__heigh * self.__base) / 2

    def triangle_perimeter(self):
        catet = sqrt((self.__base / 2) ** 2 + self.__heigh ** 2)
        return self.__base + catet * 2


class Pyramid:
    def __init__(self, base, high):
        self.__base = Square(base)
        self.__triangle_lst = [Triangle(base, high) for _ in range(4)]

    def __str__(self):
        return 'Данная пирамида состоит из основания со стороной {}, и {} треугольников с основанием {} и высотой {}'\
            .format(self.__base.edge, len(self.__triangle_lst), self.__triangle_lst[0].base, self.__triangle_lst[0].heigh)

    @property
    def base(self):
        return self.__base.edge

    @base.setter
    def base(self, base):
        if base <= 0:
            raise ValueError('Недопустимая длина основания')
        else:
            self.__base = Square(base)
            self.__triangle_lst = [Triangle(base, self.__triangle_lst[0].heigh) for _ in range(4)]

    @property
    def high(self):
        return self.__triangle_lst[0].heigh

    @high.setter
    def high(self, high):
        if high <= 0:
            raise ValueError('Недопустимая длина основания')
        else:
            self.__triangle_lst = [Triangle(self.__base, high) for _ in range(4)]

    def pyramid_surface(self):
        s = self.__triangle_lst[0].triangle_area() * len(self.__triangle_lst) + self.__base.square_area()
        return s


c = Cube(2)
print(c)
c.edge = 3
print(c)
print(c.cube_surface())
p = Pyramid(5, 7)
print(p)
print(p.pyramid_surface())
p.base = 6
print(p)
print(p.pyramid_surface())


