from math import pi


class MyMath:
    @classmethod
    def circle_len(cls, radius):
        if radius < 0:
            raise ValueError('Некорректный радиус')
        else:
            return 2 * pi * radius

    @classmethod
    def circle_sq(cls, radius):
        if radius < 0:
            raise ValueError('Некорректный радиус')
        else:
            return pi * radius ** 2

    @classmethod
    def cube_volume(cls, edge):
        if edge < 0:
            raise ValueError('Некорректный радиус')
        else:
            return edge ** 3

    @classmethod
    def sphere_surface(cls, radius):
        if radius < 0:
            raise ValueError('Некорректный радиус')
        else:
            return 4 * pi * radius ** 2


res_1 = MyMath.circle_len(radius=5)
print(res_1)
res_2 = MyMath.circle_sq(radius=6)
print(res_2)
res_3 = MyMath.cube_volume(edge=7)
print(res_3)
res_4 = MyMath.sphere_surface(radius=8)
print(res_4)
