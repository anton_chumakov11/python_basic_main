class Date:
    def __init__(self, day=0, month=0, year=0):
        self.day = day
        self.month = month
        self.year = year

    def __str__(self):
        return 'День: {}\tМесяц: {}\tГод: {}'.format(
            self.day, self.month, self.year
        )

    @classmethod
    def is_date_valid(cls, date):
        date_lst = date.split('-')
        day, month, year = int(date_lst[0]), int(date_lst[1]), int(date_lst[2])
        return 0 < day <= 31 and 0 < month <= 31 and 0 < year <= 9999

    @classmethod
    def from_string(cls, date):
        date_lst = date.split('-')
        day, month, year = int(date_lst[0]), int(date_lst[1]), int(date_lst[2])
        date_new = cls(day, month, year)
        return date_new


date = Date.from_string('10-12-2077')
print(date)
print(Date.is_date_valid('10-12-2077'))
print(Date.is_date_valid('40-12-2077'))
