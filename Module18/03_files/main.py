def check_start(path):
    reject_symbols_list = list('@№$%^&*()')
    for symbol in reject_symbols_list:
        if path.startswith(symbol):
            print('Ошибка: название начинается на один из специальных символов')
            return False
    return True

def check_end(path):
    if not (path.endswith('.txt') or path.endswith('.docx')):
        print('Ошибка: неверное расширение файла. Ожидалось .txt или .docx')
    else:
        return True

def check_path(path):
    if check_start(path):
        if check_end(path):
            print('Файл назван верно.')

path = input('Название файла: ')
check_path(path)
