# alphabit = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й',
#             'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
#             'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']

alphabit_string = 'abcdefgHIJKLMNOPQRSTUVWXYZ'.lower()
alphabit = list(alphabit_string)

def check_signes(word):
    for symbol in word:
        if '/' in word:
            return True
    return False

def cezar_k(string, k):
    if k > len(alphabit):
         k %= len(alphabit)

    for i in range(len(string)):
         if string[i] in alphabit:
            index = alphabit.index(string[i])
            if index + k < len(alphabit):
                index += k
            else:
                index = index - len(alphabit) + k
                string[i] = alphabit[index]
    new_string = ''.join(string)
    return new_string

def shifting_word(string, shift):
    if shift > len(string):
        shift %= len(string)
    string_as_list = list(string)
    shifting_list = [string_as_list[i - shift]
                     if i >= shift
                     else string_as_list[len(string_as_list) - shift + i]
                     for i in range(len(string_as_list))]
    shifting_str = ''.join(shifting_list)
    return shifting_str

def shifting(string, shift):
    # list_of_word = string.split(' ')
    # shifting_string = []
    # sh = shift
    # for word in list_of_word:
    #     new_word = shifting_word(word, sh)
    #     shifting_string.append(new_word)
    # shifting_string = ' '.join(shifting_string)
    # return shifting_string
    sh = shift
    shift_sentence = []
    shift_list_of_sent = []
    for sentence in string:
        for word in sentence:
            new_word = shifting_word(word, sh)
            shift_sentence.append(new_word)
        shift_list_of_sent.append(shift_sentence)
        sh += 1
    new_list = shift_list_of_sent[0]
    return new_list



def print_text(string):
    for symbol in string:
        if symbol == '/':
            print('\n')
        else:
            print(symbol, end = '')

def sentence(string):
    list_of_word = string.split(' ')
    list_of_words_in_sen = []
    list_of_sentence = []
    for word in list_of_word:
        if not check_signes(word):
            list_of_words_in_sen.append(word)
        else:
            list_of_words_in_sen.append(word)
            list_of_sentence.append(list_of_words_in_sen)
            list_of_words_in_sen = []
    return list_of_sentence



string = list('vujgvmCfb tj ufscfu ouib z/vhm jdjuFyqm jt fscfuu uibo jdju/jnqm fTjnqm tj scfuuf ibou fy/dpnqm yDpnqmf jt cfuufs boui dbufe/dpnqmj uGmb tj fuufsc ouib oftufe/ bstfTq jt uufscf uibo otf/ef uzSfbebcjmj vout/dp djbmTqf dbtft (ubsfo djbmtqf hifopv up csfbl ifu t/svmf ipvhiBmu zqsbdujdbmju fbutc uz/qvsj Fsspst tipvme wfsof qbtt foumz/tjm omfttV mjdjumzfyq odfe/tjmf Jo fui dfgb pg hvjuz-bncj gvtfsf fui ubujpoufnq up ftt/hv Uifsf vmetip fc pof.. boe sbcmzqsfgf zpom pof pvt..pcwj xbz pu pe ju/ Bmuipvhi uibu bzx bzn puo cf wjpvtpc bu jstug ttvomf sfzpv( i/Evud xOp tj scfuuf ibou /ofwfs uipvhiBm fsofw jt fopgu cfuufs boui iu++sjh x/op gJ ifu nfoubujpojnqmf tj eibs pu mbjo-fyq tju( b bec /jefb Jg fui foubujpojnqmfn jt fbtz up bjo-fyqm ju znb cf b hppe jefb/ bnftqbdftO bsf pof ipoljoh sfbuh efbj .. fu(tm pe psfn gp tf"uip'.lower())

string1 = cezar_k(string, 25)
string2 = sentence(string1)
string3 = shifting(string2, 3)
# string4 = sentence_in_text(string3)
string4 = ' '.join(string3)
print_text(string4)





# def cezar_circule(string):
#     for k in range(0, 26):
#         if k > len(alphabit):
#             k %= len(alphabit)
#
#         for i in range(len(string)):
#             if string[i] in alphabit:
#                 index = alphabit.index(string[i])
#                 if index + k < len(alphabit):
#                     index += k
#                 else:
#                     index = index - len(alphabit) + k
#                 string[i] = alphabit[index]
#         print(''.join(string))
#
# def cezar_k(string, k):
#     if k > len(alphabit):
#          k %= len(alphabit)
#
#     for i in range(len(string)):
#          if string[i] in alphabit:
#             index = alphabit.index(string[i])
#             if index + k < len(alphabit):
#                 index += k
#             else:
#                 index = index - len(alphabit) + k
#                 string[i] = alphabit[index]
#     new_string = ''.join(string)
#     return new_string
#
# def cezar_sentence(string, k):
#     list = string.split('/')
#     new_list = []
#     for sentenc in list:
#         new_sent = cezar_k(sentenc, k)
#         new_list.append(new_sent)
#     new_string = '/'.join(new_list)
#     return new_string
#
#
# def shifting_word(string, shift):
#     if shift > len(string):
#         shift %= len(string)
#     string_as_list = list(string)
#     shifting_list = [string_as_list[i - shift]
#                      if i >= shift
#                      else string_as_list[len(string_as_list) - shift + i]
#                      for i in range(len(string_as_list))]
#     shifting_str = ''.join(shifting_list)
#     return shifting_str
#
# def shifting(string, shift):
#     list_of_word = string.split(' ')
#     shifting_string = []
#     for word in list_of_word:
#         new_word = shifting_word(word, shift)
#         shifting_string.append(new_word)
#     shifting_string = ' '.join(shifting_string)
#     return shifting_string
#
# def print_text(string):
#     for symbol in string:
#         if symbol == '/':
#             print('\n')
#         else:
#             print(symbol, end = '')
#
# string = list('vujgvmCfb tj ufscfu ouib z/vhm jdjuFyqm jt fscfuu uibo jdju/jnqm fTjnqm tj scfuuf ibou fy/dpnqm yDpnqmf jt cfuufs boui dbufe/dpnqmj uGmb tj fuufsc ouib oftufe/ bstfTq jt uufscf uibo otf/ef uzSfbebcjmj vout/dp djbmTqf dbtft (ubsfo djbmtqf hifopv up csfbl ifu t/svmf ipvhiBmu zqsbdujdbmju fbutc uz/qvsj Fsspst tipvme wfsof qbtt foumz/tjm omfttV mjdjumzfyq odfe/tjmf Jo fui dfgb pg hvjuz-bncj gvtfsf fui ubujpoufnq up ftt/hv Uifsf vmetip fc pof.. boe sbcmzqsfgf zpom pof pvt..pcwj xbz pu pe ju/ Bmuipvhi uibu bzx bzn puo cf wjpvtpc bu jstug ttvomf sfzpv( i/Evud xOp tj scfuuf ibou /ofwfs uipvhiBm fsofw jt fopgu cfuufs boui iu++sjh x/op gJ ifu nfoubujpojnqmf tj eibs pu mbjo-fyq tju( b bec /jefb Jg fui foubujpojnqmfn jt fbtz up bjo-fyqm ju znb cf b hppe jefb/ bnftqbdftO bsf pof ipoljoh sfbuh efbj .. fu(tm pe psfn gp tf"uip'.lower())
# # k = int(input('Введите сдвиг: '))
# string1 = cezar_sentence(string, 25)
# string1_1 = shifting(string1, 3)
# print_text(string1_1)
#
#
# def sentence_in_text(list):
#     new_list = list[0]
#     # new_list = [list[i][j] for i in range(4) for j in range(len(list))]
#     return new_list

