def shifting(string, shift):
    if shift > len(string):
        shift %= len(string)
    string_as_list = list(string)
    shifting_list = [string_as_list[i - shift]
                     if i >= shift
                     else string_as_list[len(string_as_list) - shift + i]
                     for i in range(len(string_as_list))]
    shifting_str = ''.join(shifting_list)
    return shifting_str

def is_same(string1, string2):
    for i in range(1, len(string1)):
        string1 = shifting(string1, 1)
        # print(string1)
        if string1 == string2:
            print('Первая строка получается из второй со сдвигом', i)
            return True
    if string1 == string2:
        print('Первая строка получается из второй со сдвигом 0')
        return True
    print('Первую строку нельзя получить из второй с помощью циклического сдвига.')
    return False



string1 = input('Первая строка: ')
string2 = input('Вторая строка: ')

is_same(string1, string2)


