def chek_point(list):
    if len(list) == 4:
        return True
    else:
        print('Адрес - это четыре числа, разделенные точками')
        return False

def chek_num(list):
    for elem in list:
        if not elem.isdigit():
            print(elem, '- не целое число')
            return False
    return True

def chek_range(list):
    for elem in list:
        if int(elem) > 255:
            print(elem, 'превышает 255')
            return False
    return True

def chek_adress(string):
    adress_as_list = adress.split('.')
    if chek_point(adress_as_list) \
            and chek_num(adress_as_list)\
            and chek_range(adress_as_list):
        print('IP-адрес корректен')

adress = input('Введите IP: ')
chek_adress(adress)
