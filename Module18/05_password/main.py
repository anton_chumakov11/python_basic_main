def check_length(passw):
    if len(passw) < 8:
        return False
    return True

def check_num(passw):
    string_of_numbers = '0123456789'
    num_counter = 0
    for symbol in passw:
        if symbol in string_of_numbers:
            num_counter += 1
    if num_counter < 3:
        return False
    return True

def check_up(passw):
    for symbol in passw:
        if symbol.isupper():
            return True
    return False

def check_pass(passw):
    if check_length(passw) and check_num(passw) and check_up(passw):
        return True
    return False

passw = input('Придумайте пароль: ')
while not check_pass(passw):
    print('Пароль ненадёжный. Попробуйте ещё раз.')
    passw = input('Придумайте пароль: ')
print('Это надёжный пароль!')
