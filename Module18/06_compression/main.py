def compression(string):
    sym = string[0]
    counter = 0
    compress_string = ''
    for symbol in string:
        if symbol == sym:
            counter += 1
        else:
            compress_string += str(counter) + sym
            counter = 1
        sym = symbol
    compress_string += str(counter) + sym
    return compress_string

string = input('Введите строку: ')
compress_string = compression(string)
print(compress_string)
