input_string = input('Введите строку: ').split(' ')
list_of_length = []

for word in input_string:
    list_of_length.append(len(word))

max_length = list_of_length[0]
for i_length in range(len(list_of_length)):
    if list_of_length[i_length] > max_length:
        max_length = list_of_length[i_length]

print('Самое длинное слово -', input_string[list_of_length.index(max_length)],
      ', его длина -', max_length)


