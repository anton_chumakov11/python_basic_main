import math as m


class Car:
    def __init__(self, x, y, angle):
        self.x = x
        self.y = y
        self.angle = angle

    def drive(self, distance, new_angle):
        self.x = round(self.x + distance * m.sin(m.radians(new_angle)))
        self.y = round(self.y + distance * m.sin(m.radians(new_angle)))


class Bus(Car):
    def __init__(self, x, y, angle):
        super().__init__(x, y, angle)
        self.__passengers = 0
        self.__money = 0

    def input(self, input_passengers):
        self.__passengers += input_passengers

    def output(self, output_passengers):
        if self.__passengers < output_passengers:
            self.__passengers = 0
        else:
            self.__passengers -= output_passengers

    def drive(self, distance, new_angle):
        self.x = round(self.x + distance * m.sin(m.radians(new_angle)))
        self.y = round(self.y + distance * m.sin(m.radians(new_angle)))
        self.__money += self.__passengers * distance


