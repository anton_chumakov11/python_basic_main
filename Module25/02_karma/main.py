from exceptions import *
from random import randint
GOOD_KARMA = 500


def one_day():
    karma_for_day = randint(1, 7)
    probability = randint(1, 10)
    random_exception = randint(1, 5)
    if probability == 1:
        if random_exception == 1:
            raise KillError()
        elif random_exception == 2:
            raise DrunkError()
        elif random_exception == 3:
            raise CarCrashError()
        elif random_exception == 4:
            raise GluttonyError()
        elif random_exception == 5:
            raise DepressionError()
    return karma_for_day


karma = 0

while True:
    file = open('karma.log', 'a', encoding='utf-8')
    try:
        karma += one_day()
    except KillError:
        file.write('KillError\n')
    except DrunkError:
        file.write('DrunkError\n')
    except CarCrashError:
        file.write('CarCrashError\n')
    except GluttonyError:
        file.write('GluttonyError\n')
    except DepressionError:
        file.write('DepressionError\n')
    if karma >= GOOD_KARMA:
        file.close()
        break
print('Вы достигли просветления!')

