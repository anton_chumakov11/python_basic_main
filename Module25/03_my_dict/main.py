class MyDict(dict):
    def get(self, key):
        if key not in self.keys():
            return 0
        else:
            return self[key]


dct1 = {1: 1, 2: 2, 3: 3}
dct2 = MyDict()
dct2[1] = 1
dct2[2] = 2
print(dct1.get(4))
print(dct2.get(4))
print(dct1.get(1))
print(dct2.get(1))
