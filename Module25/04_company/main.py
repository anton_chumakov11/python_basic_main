from employees import *

manager1 = Manager()
manager2 = Manager()
manager3 = Manager()
agent1 = Agent(sales=50000)
agent2 = Agent(sales=75000)
agent3 = Agent(sales=100000)
worker1 = Worker(work_hours=120)
worker2 = Worker(work_hours=160)
worker3 = Worker(work_hours=180)
employees_lst = [manager1, manager2, manager3, agent1, agent2, agent3, worker1, worker2, worker3]

total_salary = 0
for i_employee in employees_lst:
    total_salary += i_employee.salary_calculation()
    print(i_employee)
print(total_salary)


