class Person:
    def __init__(self, name='Bill', surname='Jonson', age=40):
        self.__name = name
        self.__surname = surname
        self.__age = age

    def __str__(self):
        return 'name: {}\nsurname: {}\nage: {}'.format(self.__name, self.__surname, self.__age)


class Employee(Person):
    def salary_calculation(self):
        pass


class Manager(Employee):
    def salary_calculation(self):
        return 1300

    def __str__(self):
        info = super().__str__()
        info = '\n'.join(
            (
                info,
                'Position: Manager\nSalary: {}\n'.format(self.salary_calculation())
            )
        )
        return info


class Agent(Employee):
    def __init__(self, sales, name='Bill', surname='Jonson', age=40):
        super().__init__(name, surname, age)
        self.sales = sales

    def salary_calculation(self):
        return 5000 + self.sales * 0.05

    def __str__(self):
        info = super().__str__()
        info = '\n'.join(
            (
                info,
                'Position: Agent\nSalary: {}\n'.format(self.salary_calculation())
            )
        )
        return info


class Worker(Employee):
    def __init__(self, work_hours, name='Bill', surname='Jonson', age=40):
        super().__init__(name, surname, age)
        self.work_hours = work_hours

    def salary_calculation(self):
        return self.work_hours * 100

    def __str__(self):
        info = super().__str__()
        info = '\n'.join(
            (
                info,
                'Position: Worker\nSalary: {}\n'.format(self.salary_calculation())
            )
        )
        return info
