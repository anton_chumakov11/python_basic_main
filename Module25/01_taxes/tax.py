class Property:
    def __init__(self, worth):
        self.worth = worth

    def tax_count(self):
        return self.worth


class Apartment(Property):
    def __init__(self, worth):
        super().__init__(worth)

    def tax_count(self):
        return self.worth / 1000


class Car(Property):
    def __init__(self, worth):
        super().__init__(worth)

    def tax_count(self):
        return self.worth / 200


class CountryHouse(Property):
    def __init__(self, worth):
        super().__init__(worth)

    def tax_count(self):
        return self.worth / 500

