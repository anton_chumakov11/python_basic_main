import tax as t


def property_tax():
    property_type = input('Введите тип имущества\n'
                          '1 - автомобиль, 2 - квартира, 3 - дача: ')
    while property_type not in '123':
        print('Повторите ввод!')
        property_type = input('Введите тип имущества\n'
                              '1 - автомобиль, 2 - квартира, 3 - дача: ')
    property_type = int(property_type)
    property_worth = input('Введите стоимость имущества: ')
    while not property_worth.isdigit():
        print('Повторите ввод!')
        property_worth = input('Введите стоимость имущества: ')
    property_worth = int(property_worth)
    if property_type == 1:
        current_property = t.Car(property_worth)
        current_property_tax = current_property.tax_count()
    elif property_type == 2:
        current_property = t.Apartment(property_worth)
        current_property_tax = current_property.tax_count()
    else:
        current_property = t.CountryHouse(property_worth)
        current_property_tax = current_property.tax_count()
    return current_property_tax


def tax_counter():
    full_tax = 0
    money = input('Введите количество денег: ')
    while not money.isdigit():
        print('Введите стоимость цифрами!')
        money = input('Введите количество денег: ')
    money = int(money)
    answer = 1
    while answer == 1:
        full_tax += property_tax()
        answer = input('Хотите внести ещё имущество?\n'
                       '1 - да, 2 - нет: ')
        while answer not in '12':
            answer = input('Некорректный ввод\n'
                           'Хотите внести ещё имущество?\n'
                           '1 - да, 2 - нет: ')
        answer = int(answer)
    print('Вам нужно заплатить {} налога'.format(full_tax))
    if full_tax > money:
        print('Вам не хватает: ', full_tax - money)


tax_counter()
