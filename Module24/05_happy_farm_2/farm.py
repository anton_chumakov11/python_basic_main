class Potato:
    readines = {0: 'Нет', 1: 'Росток', 2: 'Зеленая', 3: 'Зрелая'}

    def __init__(self, index):
        self.index = index
        self.state = 0

    def grow(self):
        if self.state < 3:
            self.state += 1
        else:
            print('Картошка под номером {} уже созрела!'.format(self.index))

    def status(self):
        print('Картошка под номером {} в стадии {}'.format(self.index, Potato.readines[self.state]))


class PotatoesSet:
    def __init__(self, count):
        self.potatoes_lst = [Potato(i_index) for i_index in range(1, count + 1)]

    def grow(self):
        for i_potato in self.potatoes_lst:
            i_potato.grow()
        print('Картошка проросла')

    def status(self):
        for i_potato in self.potatoes_lst:
            i_potato.status()

    def grow_status(self):
        grow_status_dict = dict()
        for i_potato in self.potatoes_lst:
            grow_status_dict[i_potato.index] = i_potato.state
        return grow_status_dict

    def harvest(self):
        print('Идем за урожаем')
        grow_status = self.grow_status().values()
        if all(i_elem==3 for i_elem in grow_status):
            self.potatoes_lst = []
            print('Картошка собрана')
        else:
            print('Картошку рано собирать!')
            self.status()


class Farmer:
    def __init__(self, name='John', count=10):
        self.name = name
        self.garden = PotatoesSet(count)

    def work_in_garden(self):
        self.garden.grow()

    def harvest(self):
        self.garden.harvest()


