import elements as e

water = e.Water()
air = e.Air()
fire = e.Fire()
ground = e.Ground()

print(water + air)
print(water + fire)
print(water + ground)
print(air + fire)
print(air + ground)
print(fire + ground)
