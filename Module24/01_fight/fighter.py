import random as r

class Fighter:
    health = 100

    def damage(self):
        self.health -= 20



class Fight:

    def __init__(self):
        self.fighters_lst = [Fighter() for _ in range(0, 2)]

    def hit(self):
        fighter_index = r.randint(0, 1)
        self.fighters_lst[fighter_index].damage()
        print('Воин {} атаковал, у воина {} осталось {} здоровья'.format(
            abs(fighter_index - 1) + 1, fighter_index + 1, self.fighters_lst[fighter_index].health
        ))

    def fighting(self):
        while (self.fighters_lst[0].health > 0) and (self.fighters_lst[1].health > 0):
            self.hit()
        if self.fighters_lst[0].health == 0:
            print('Воин 2 одержал победу!')
        else:
            print('Воин 1 одержал победу!')
