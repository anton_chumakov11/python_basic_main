from student import Student
import random, collections

students_list = list()
for i_index in range(10):
    students_list.append(Student('', random.randint(1, 10), []))
    students_list[i_index].gen_ran_ns()
    students_list[i_index].gen_ran_appraisals()

students_dict = dict()
for i_student in students_list:
    students_dict[i_student] = i_student.avarage_score()

sorted_values = sorted(students_dict.values())
sorted_students_dict = collections.OrderedDict()
for i_values in sorted_values:
    for j_keys in students_dict.keys():
        if students_dict[j_keys] == i_values:
            sorted_students_dict[j_keys] = students_dict[j_keys]


i_index = 0
for i_keys in sorted_students_dict.keys():
    students_list[i_index] = i_keys
    i_index += 1

for i_student in students_list:
    print(i_student.get_info(), 'Сердний балл: ', i_student.avarage_score(), '\n')

