from statistics import mean
import random, string

class Student:

    def __init__(self, ns, group_namber, appraisals):
        self.ns = ns
        self.group_namber = group_namber
        self.appraisals = appraisals

    def gen_ran_appraisals(self):
        for _ in range(5):
            self.appraisals.append(random.randint(1, 5))

    def avarage_score(self):
            return mean(self.appraisals)

    def gen_ran_ns(self):
        letters = string.ascii_lowercase
        self.ns = ''.join(random.choice(letters) for _ in range(5)) + ' ' +\
                  ''.join(random.choice(letters) for _ in range(5))

    def get_info(self):
        print('Имя и фамилия: {}\nНомер группы: {}\nОценки: {}'.format(
            self.ns, self.group_namber, self.appraisals
        ))
