from family import Parent

parent1 = Parent('Mike', 15)

parent1.give_info()
parent1.calm_down_child()
parent1.feed_child()
parent1.give_info()
