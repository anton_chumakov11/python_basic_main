from random import randint, choice
from string import ascii_lowercase


class Child:
    def __init__(self, name='Baby', age=1, calm_state=False, hungry_state=True):
        self.name = name
        self.age = age
        self.calm_state = calm_state
        self.hungry_state = hungry_state


class Parent:
    def __init__(self, name='Parent', age=40):
        self.name = name
        self.age = age
        while self.age < 17:
            print('Родитель слишком молодой!')
            self.age = int(input('Родитель слишком молодой! Введите возраст больше 16: '))
        self.list_of_child = self.list_of_child_genetate()

    def list_of_child_genetate(self):
        count_of_child = int(input('Введите кол-во детей: '))
        list_of_child = []
        for i_index in range(count_of_child):
            list_of_child.append(Child(''.join(choice(ascii_lowercase) for _ in range(5)), randint(1, self.age - 16)))
        return list_of_child

    def give_info(self):
        print(
            'Имя: {}\nВозраст: {}\nСписок детей:'.format(
                self.name, self.age
            )
        )
        for i_child in self.list_of_child:
            print(
                ' {} ребенок:\n  Имя: {}\n  Возраст: {}\n  Спокоен? {}\n  Голоден? {}'.format(
                    self.list_of_child.index(i_child) + 1, i_child.name, i_child.age, i_child.calm_state, i_child.hungry_state
                )
            )

    def calm_down_child(self):
        index = int(input('Какого ребенка успокоить? '))
        while index > len(self.list_of_child):
            print('Такого ребёнка нет! Введите номер не больше {}'.format(len(self.list_of_child)), end=' ')
            index = int(input())
        if self.list_of_child[index - 1].calm_state:
            print('Ребёнок и так спокоен')
        else:
            self.list_of_child[index - 1].calm_state = True
            print('Ребенок успокоился')

    def feed_child(self):
        index = int(input('Какого ребенка покормить? '))
        while index > len(self.list_of_child):
            print('Такого ребёнка нет! Введите номер не больше {}'.format(len(self.list_of_child)), end=' ')
            index = int(input())
        if self.list_of_child[index - 1].hungry_state:
            print('Ребёнок и так сыт')
        else:
            self.list_of_child[index - 1].hungry_state = True
            print('Ребенок наелся')
