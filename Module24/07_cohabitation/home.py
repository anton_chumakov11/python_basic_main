from random import randint


class Person:
    def __init__(self, name, home):
        self.name = name
        self.home = home
        self.satiety = 50

    def person_logic(self):
        counter = 0
        while counter <= 365:
            dice = randint(1, 6)
            if self.satiety < 0:
                print(
                    '{} умер('.format(self.name)
                )
                break
            elif self.satiety < 20:
                self.eat()
            elif self.home.fridge < 10:
                self.shopping()
            elif self.home.money < 50:
                self.work()
            elif dice == 1:
                self.work()
            elif dice == 2:
                self.eat()
            else:
                self.play()
            counter += 1
        else:
            print('{} выжил!'.format(self.name))

    def eat(self):
        if self.home.fridge >= 10:
            self.home.fridge -= 10
            self.satiety += 10
            print(
                '{} поел, в холодильнике {} еды, сытость - {}'.format(
                    self.name, self.home.fridge, self.satiety
                )
            )
        else:
            print('Холодильник пустой, еды нет!')

    def shopping(self):
        if self.home.money == 0:
            print('Денег нет, еду купить не на что!')
        else:
            money = self.home.money
            self.home.fridge += money
            self.home.money -= money
            print(
                '{} купил {} еды, в холодильнике теперь {} еды'.format(
                    self.name, money, self.home.fridge
                )
            )

    def work(self):
        self.satiety -= 25
        self.home.money += 75
        print(
            '{} заработал 50 денег. Сытость - {}'.format(
                self.name, self.satiety
            )
        )

    def play(self):
        self.satiety -= 10
        print(
            '{} поиграл, сытость - {}'.format(
                self.name, self.satiety
            )
        )


class Home:
    fridge = 50
    money = 0
