import home as h

house = h.Home()
user1 = h.Person('Артём', house)
user2 = h.Person('Оля', house)

user1.person_logic()
user2.person_logic()
