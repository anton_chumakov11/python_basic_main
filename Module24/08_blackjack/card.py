import random

class DeckOfCard:
    def __init__(self):
        self.card_lst = []
        card = 2
        counter = 0
        for _ in range(40):
            self.card_lst.append(card)
            counter += 1
            if counter == 4:
                card += 1
                counter = 0
        for _ in range(12):
            self.card_lst.append(10)
        random.shuffle(self.card_lst)

    def shuffle(self):
        self.card_lst = []
        card = 2
        counter = 0
        for _ in range(40):
            self.card_lst.append(card)
            counter += 1
            if counter == 4:
                card += 1
                counter = 0
        for _ in range(12):
            self.card_lst.append(10)
        random.shuffle(self.card_lst)


class User:
    def __init__(self, deck):
        self.hand = []
        self.deck = deck

    def get_card(self):
        if len(self.deck.card_lst) == 0:
            print('В колоде кончились карты!')
        else:
            # index = random.randint(0, len(self.deck.card_lst) - 1)
            self.hand.append(self.deck.card_lst[0])
            del self.deck.card_lst[0]

    def show_card(self):
        print('Карты на руке:')
        for i_card in self.hand:
            print('{},'.format(i_card), end=' ')
        print('Количество очков: {}'.format(sum(self.hand)))


class CompUser:
    def __init__(self, deck):
        self.hand = []
        self.deck = deck

    def get_card(self):
        # if len(self.deck.card_lst) == 0:
        # index = random.randint(0, len(self.deck.card_lst) - 1)
        self.hand.append(self.deck.card_lst[0])
        del self.deck.card_lst[0]

    def play(self):
        while sum(self.hand) <= 19:
            self.get_card()

    def show_card(self):
        print('Карты компьютера:')
        for i_card in self.hand:
            print('{},'.format(i_card), end=' ')
        print('Количество очков: {}'.format(sum(self.hand)))


class BlackJack:
    def __init__(self, user, comp_user):
        self.user = user
        self.comp_user = comp_user

    def play(self):
        self.game()
        action = int(input('Хотите продолжить? 1-да, 2-нет: '))
        while action == 1:
            self.checking_card()
            self.game()
            action = int(input('Хотите продолжить? 1-да, 2-нет: '))


    def game(self):
        self.user.hand = []
        self.comp_user.hand = []
        self.user.get_card()
        self.user.get_card()
        self.comp_user.get_card()
        self.comp_user.get_card()
        self.user.show_card()
        while True:
            print('Выберите действие:\n1 - добрать карту\n2 - остановиться: ')
            action = int(input())
            if action == 1:
                self.user.get_card()
                self.user.show_card()
            if action == 2:
                break
        self.comp_user.play()
        print('Вскрываемся!')
        self.user.show_card()
        self.comp_user.show_card()
        self.result()

    def result(self):
        user_result = sum(self.user.hand)
        comp_result = sum(self.comp_user.hand)

        if user_result == comp_result:
            print('Ничья!')
        elif user_result > 21 and comp_result > 21:
            print('Оба проиграли')
        elif user_result <= 21 and comp_result > 21:
            print('Вы выйграли')
        elif user_result > 21 and comp_result <= 21:
            print('Вы проиграли')
        else:
            if (21 - user_result) < (21 - comp_result):
                print('Вы выйграли')
            else:
                print('Вы проиграли')

    def checking_card(self):
        if len(self.user.deck.card_lst) < 5:
            print('В колоде кончились карты! Колода перемешивается заново.')
        self.user.deck.shuffle()
