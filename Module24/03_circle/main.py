from circle import Circle

c1 = Circle(0, 0, 2)
c2 = Circle(3, 3, 3)
c3 = Circle(-4, -3, 2)

print(c1.is_intersects(c2))
print(c1.is_intersects(c3))

print(c1.square())
c1.increase(2)
print(c1.square())
