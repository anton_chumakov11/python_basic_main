from math import pi, sqrt

class Circle:

    def __init__(self, x=0, y=0, r=1):
        self.x = x
        self.y = y
        self.r = r

    def square(self):
        s = pi * self.r ** 2
        return s

    def perimetr(self):
        p = 2 * pi * self.r

    def increase(self, index=1):
        self.r *= index

    def is_intersects(self, another_circle):
        distance = sqrt((abs(self.x - another_circle.x) ** 2 +
                        abs(self.y - another_circle.y) ** 2))
        if distance <= (self.r + another_circle.r):
            return True
        else:
            return False
