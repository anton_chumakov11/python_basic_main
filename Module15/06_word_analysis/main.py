def get_list_of_uniq_letters(list):
    list_of_uniq_letters = []
    counter_letter_in_word = 0
    for i in list:
        for j in list:
            if i == j:
              counter_letter_in_word += 1
        if counter_letter_in_word == 1:
            list_of_uniq_letters.append(i)
        counter_letter_in_word = 0
    return list_of_uniq_letters


word = input('Введите слово: ')

word_as_list = list(word)
print('Количество уникальных букв:', len(get_list_of_uniq_letters(word_as_list)))
