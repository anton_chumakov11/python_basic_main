number_of_cell = int(input('Кол-во клеток: '))
list_of_cell = []
list_of_good_cell = []
list_of_index_of_good_cell = []


for i in range(number_of_cell):
    print('Эффективность', i + 1, 'клетки:', end = ' ')
    cell = int(input())
    list_of_cell.append(cell)
    if cell < i + 1:
        list_of_good_cell.append(cell)
        list_of_index_of_good_cell.append(i + 1)

print('Неподходящие значения:')

for i in range(len(list_of_good_cell)):
    print( list_of_good_cell[i], list_of_index_of_good_cell[i])


