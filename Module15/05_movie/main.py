def is_film_in_list(list_of_films, users_film):
    film_in_list = False
    for i in list_of_films:
        if i == users_film:
            film_in_list = True
    return film_in_list

def favourite_film_add(film, film_list, favourite_film_list):
    if is_film_in_list(film_list, film) == True:
        if is_film_in_list(favourite_film_list, film) == True:
            print('Данный фильм уже в списке любимых')
            print('Список любимых фильмов:', favourite_films)
        else:
            favourite_film_list.append(film)
            print('Список любимых фильмов:', favourite_films)
    elif film == 'end':
        print('Программа завершена')
        print('Список любимых фильмов:', favourite_films)
    else:
        print('Данного фильма нет в базе')
        print('Список любимых фильмов:', favourite_films)



films = ['Крепкий орешек', 'Назад в будущее', 'Таксист',
         'Леон', 'Богемская рапсодия', 'Город грехов',
         'Мементо', 'Отступники', 'Деревня']
favourite_films = []

users_film = ''
while users_film != 'end':
    users_film = input('Введите название фильма для добавления в список любымых или end для завершения программы: ')
    favourite_film_add(users_film, films, favourite_films)


