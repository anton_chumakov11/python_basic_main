shop = [['каретка', 1200], ['шатун', 1000], ['седло', 300],
        ['педаль', 100], ['седло', 1500], ['рама', 12000],
        ['обод', 2000], ['шатун', 200], ['седло', 2700]]

detail = input('Название детали: ')

detail_count = 0
detail_cost = 0
for i_product in shop:
        if i_product[0] == detail:
                detail_count += 1
                detail_cost += i_product[1]

if detail_count == 0:
        print('Данной детали нет в магазине')
else:
        print('Кол-во деталей -', detail_count)
        print('Общая стоимость -', detail_cost)
