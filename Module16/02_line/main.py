def  sorting(list):
    for i in range(len(list)):
        for j in range(len(list)):
            if list[i] < list[j]:
                list[i], list[j] = list[j], list[i]

first_class = list(range(160, 177, 2))
second_class = list(range(162, 181, 3))
print('Первый класс:', first_class, '\nВторой класс:', second_class)

students = first_class
students.extend(second_class)

sorting(students)
print('Все ученики по росту:', students)



