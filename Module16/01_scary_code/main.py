a = [1, 5, 3]
b = [1, 5, 1, 5]
c = [1, 3, 1, 5, 3, 3]

# for i in b:
#     a.append(i)
a.extend(b)

# t = 0
# for i in a:
#     if i == 5:
#         t += 1
# print(t)
count_of_5 = a.count(5)
print('Кол-во цифр 5 при первом объединении:', count_of_5)

# d = []
# for i in a:
#     if i != 5:
#         d.append(i)
for _ in range(count_of_5):
    a.remove(5)

# for i in c:
#     d.append(i)
a.extend(c)

# t = 0
# for i in d:
#     if i == 3:
#         t += 1
# print(t)
count_of_3 = a.count(3)
print('Кол-во цифр 3 при втором объединении:', count_of_3)

# print(d)
print('Итоговый список: ', a)


