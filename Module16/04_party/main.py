def party(list):
    print('Сейчас на вечеринке', len(list), 'человек:', list)
    event = input('Гость пришёл или ушёл? ')
    if event == 'пришёл':
        came(list)
    elif event == 'ушёл':
        gone(list)
    elif event == 'Пора спать':
        sleep()

def came(list):
    name = input('Имя гостя: ')
    if len(list) < 6:
        list.append(name)
        print('Привет,', name + '!')
        party(list)
    else:
        print('Прости,', name + ', но мест нет.')
        party(list)

def gone(list):
    name = input('Имя гостя: ')
    if list.count(name) > 0:
        list.remove(name)
        print('Пока,', name)
    else:
        print('Такого гостя нет на вечеринке!')
    party(list)

def sleep():
    print('Вечеринка закончилась, все легли спать.')

guests = ['Петя', 'Ваня', 'Саша', 'Лиза', 'Катя']

party(guests)


