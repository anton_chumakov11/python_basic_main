violator_songs = [
    ['World in My Eyes', 4.86],
    ['Sweetest Perfection', 4.43],
    ['Personal Jesus', 4.56],
    ['Halo', 4.9],
    ['Waiting for the Night', 6.07],
    ['Enjoy the Silence', 4.20],
    ['Policy of Truth', 4.76],
    ['Blue Dress', 4.29],
    ['Clean', 5.83]
]


count_of_songs = int(input('Сколько песен выбрать? '))
while count_of_songs > 9 or count_of_songs < 0:
    count_of_songs = int(input('В списке только 9 песен. Сколько из них выбрать? '))
favourite_songs = []
time = 0
for i in range(count_of_songs):
    flag = 0
    print('Название', i + 1, 'песни:', end = ' ')
    song = input()
    while song in favourite_songs:
        print('Данная песня уже в списке избранных. Введите название', i + 1, 'песни ещё раз:', end = ' ')
        song = input()
    for i_songs in violator_songs:
        if i_songs[0] == song:
            favourite_songs.append(song)
            time += i_songs[1]
            flag = 1
    if flag == 0:
        print('Данной песни нет в списке')
print('Общее время звучания песен:', time, 'минут')

