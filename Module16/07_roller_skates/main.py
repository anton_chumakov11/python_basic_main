def fill_list_skate(list):
    count = int(input('Кол-во коньков: '))
    for i in range(count):
        print('Размер', i + 1, 'пары:', end = ' ')
        item = int(input())
        list.append(item)

def fill_list_legs(list):
    count = int(input('Кол-во людей: '))
    for i in range(count):
        print('Размер ноги', i + 1, 'человека:', end = ' ')
        item = int(input())
        list.append(item)

def  sorting(list):
    for i in range(len(list)):
        for j in range(len(list)):
            if list[i] < list[j]:
                list[i], list[j] = list[j], list[i]

def how_mach_can_take(list_of_skate, list_of_legs):
    sorting(list_of_skate)
    count = 0
    for i_leg in list_of_legs:
        for i_skate in list_of_skate:
            if i_skate >= i_leg:
                list_of_skate.remove(i_skate)
                count += 1
                break
    return count


skate_list = []
leg_list = []

fill_list_skate(skate_list)
print()
fill_list_legs(leg_list)

print('Наибольшее кол-во людей, которые могут взять ролики:', how_mach_can_take(skate_list, leg_list))


