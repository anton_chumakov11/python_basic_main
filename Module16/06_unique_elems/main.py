def fill_list(list, count):
   for i in range(count):
    print('Введите', i + 1, 'элемент списка:', end = ' ')
    item = int(input())
    list.append(item)

first_list = []
second_list = []

print('Первый список:')
fill_list(first_list, 3)
print('\nВторой список:')
fill_list(second_list, 7)

first_list.extend(second_list)

# list_of_count = []
#
# for i_item in first_list:
#     list_of_count.append(first_list.count(i_item))

# for i in range(len(first_list)):
#     for j in range(list_of_count[i]):
#         first_list.remove(first_list[i])

for i_item in first_list:
    while first_list.count(i_item) > 1:
        first_list.remove(i_item)

print('Новый первый список с уникальными элементами:', first_list)

