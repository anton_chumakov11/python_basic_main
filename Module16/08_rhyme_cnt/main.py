def rhyme(number_of_people, number_in_rhyme):
    people_list = list(range(1, number_of_people + 1))
    start = 0

    while len(people_list) > 1:
        number_of_people = len(people_list)
        print('Текущий круг людей:', people_list)
        if start < len(people_list):
            print('Начало счёта с номера', people_list[start])
        else:
            start %= len(people_list)
            print('Начало счёта с номера', people_list[start])
        retired = start + number_in_rhyme % number_of_people - 1
        if retired < len(people_list):
            print('Выбывает человек под номером', people_list[retired], '\n')
        else:
            retired %= len(people_list)
            print('Выбывает человек под номером', people_list[retired], '\n')
        people_list.remove(people_list[retired])
        start = retired
    print('\nОстался человек под номером', people_list[start])


number_of_people = int(input('Кол-во человек: '))
number_in_rhyme = int(input('Какое число в считалке? '))
print('Значит, выбывает каждый', number_in_rhyme, 'человек\n')

rhyme(number_of_people, number_in_rhyme)


