def fill_list(list, count):
    for _ in range(count):
        n = int(input('Число: '))
        list.append(n)

def print_list(list):
    for i_elem in list:
        print(i_elem, end = ' ')

def is_palindrom(list):
    new_list = []
    for i in range(len(list) - 1, -1, -1):
        new_list.append(list[i])
    if list == new_list:
        return True
    else:
        return False

def viceversa(list):
    new_list = []
    for i in range(len(list) - 1, -1, -1):
        new_list.append(list[i])
    return new_list


count_number = int(input('Кол-во чисел: '))

number_list = []
fill_list(number_list, count_number)

print('\nПоследовательность:', end = ' ')
print_list(number_list)

new_list = []
answer = []
for i in range(len(number_list)):
    for j in range(i, len(number_list)):
        new_list.append(number_list[j])
    if is_palindrom(new_list):
        break
    else:
        answer.append(number_list[i])
    new_list = []

answer = viceversa(answer)
print('\nНужно приписать чисел:', len(answer))
print('Сами числа:', end = ' ')
print_list(answer)
