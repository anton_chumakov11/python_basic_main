class Node:
    def __init__(self, value=None, link=None):
        self.value = value
        self.link = link

    def __str__(self):
        return str(self.value)


class SinglyLinkedList:
    def __init__(self):
        self.head_node = None
        self.length = 0
        self.counter = 0

    def __str__(self):
        if self.head_node is not None:
            current_node = self.head_node
            nodes_lst = [str(current_node.value)]
            while current_node.link is not None:
                current_node = current_node.link
                nodes_lst.append(str(current_node.value))
            return '[{}]'.format(', '.join(nodes_lst))
        return '[]'

    def append(self, elem):
        new_node = Node(elem)
        if self.head_node is None:
            self.head_node = new_node
            self.length += 1
        else:
            last_node = self.head_node
            while last_node.link:
                last_node = last_node.link
            last_node.link = new_node
            self.length += 1

    def remove(self, index):
        current_node = self.head_node
        current_index = 0
        if self.length == 0 or index >= self.length:
            raise IndexError

        if current_node is not None:
            if index == 0:
                self.head_node = current_node.link
                self.length -= 1
            else:
                while current_node is not None:
                    if current_index == index:
                        break
                    prev_node = current_node
                    current_node = current_node.link
                    current_index += 1

                prev_node.link = current_node.link
                self.length -= 1

    def get(self, index):
        if self.length == 0 or index >= self.length:
            raise IndexError
        if index == 0:
            return self.head_node
        current_node = self.head_node
        current_index = 0
        while current_node is not None:
            if current_index == index:
                break
            current_node = current_node.link
            current_index += 1
        return current_node

    def __iter__(self):
        self.counter = 0
        return self

    def __next__(self):
        if self.counter == self.length:
            raise StopIteration
        current_node = self.get(self.counter)
        self.counter += 1
        return current_node


lst = SinglyLinkedList()
lst.append(1)
lst.append(2)
lst.append(3)
print(lst)
print(lst.get(1))
lst.remove(1)
print(lst)
lst.append(2)
lst.append(4)
for i_elem in lst:
    print(i_elem, end = ' ')
