import os


def gen_string_code(failname):
    file = open(failname, 'r', encoding='utf-8')
    string_code_count = 0
    for i_line in file:
        if not i_line.startswith('\n') and not i_line.startswith('#'):
            string_code_count += 1
    yield string_code_count


def gen_files_path(cur_dir=os.path.abspath(os.sep)):
    for i_elem in os.listdir(cur_dir):
        path = os.path.join(cur_dir, i_elem)
        if os.path.isfile(path):
            if i_elem.endswith('.py'):
                yield from gen_string_code(path)



file_gen = gen_files_path(os.path.abspath(os.path.join('..', '..', 'Module24', '01_fight')))

# for i_file in file_gen:
#     print(i_file)

print(sum(gen_files_path(os.path.abspath(os.path.join('..', '..', 'Module24', '01_fight')))))
