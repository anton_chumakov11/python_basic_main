from collections.abc import Iterable


class QHofstadterIter:
    def __init__(self, lst: list[int], max_n: int = 20) -> None:
        self.__counter = 1
        self.__q_prev = lst
        self.max_n = max_n

    def __iter__(self) -> Iterable[int]:
        return self

    def __next__(self) -> int:
        self.__counter += 1
        if self.__counter - 2 < self.max_n and self.__q_prev != [1, 2]:
            q_cur = self.__q_prev[self.__counter - self.__q_prev[self.__counter - 1]] + \
                    self.__q_prev[self.__counter - self.__q_prev[self.__counter - 2]]
            self.__q_prev.append(q_cur)
        else:
            raise StopIteration()
        return self.__q_prev[self.__counter - 2]


def q_hofstadter_gen(lst: list[int], max_n: int = 20) -> Iterable[int]:
    counter = 2
    if lst == [1, 1]:
        for n in range(2, max_n + 2):
            q_cur = lst[counter - lst[counter - 1]] + \
                        lst[counter - lst[counter - 2]]
            lst.append(q_cur)
            yield lst[counter - 2]
            counter += 1
    else:
        return


hof_iter = QHofstadterIter([1, 1])
for i_elem in hof_iter:
    print(i_elem, end=', ')

print()
hof_gen = q_hofstadter_gen([1, 1])
for i_elem in hof_gen:
    print(i_elem, end=', ')
