from collections.abc import Iterable


class SqrIter:
    def __init__(self, n: int) -> None:
        self.__max_elem = n
        self.__cur_elem = 0

    def __iter__(self) -> Iterable[int]:
        return self

    def __next__(self) -> int:
        if self.__cur_elem < self.__max_elem:
            self.__cur_elem += 1
        else:
            raise StopIteration()
        return self.__cur_elem ** 2


def sqr_gen(N: int) -> Iterable[int]:
    for n in range(1, N + 1):
        yield n ** 2


N = int(input('Введите число N: '))

sqr1 = SqrIter(N)
for i_elem in sqr1:
    print(i_elem)
print()

sqr2 = sqr_gen(N)
for i_elem in sqr2:
    print(i_elem)
print()

sqr3 = (n ** 2 for n in range(1, N + 1))
for i_elem in sqr3:
    print(i_elem)

