import os


def gen_files_path(user_dir, cur_dir = os.path.abspath(os.sep)):
    for i_elem in os.listdir(cur_dir):
        if i_elem == user_dir:
            print('Finded')
            return
        path = os.path.join(cur_dir, i_elem)
        if os.path.isfile(path):
            # print(path)
            yield path
        else:
            yield from gen_files_path(user_dir, path)


file_gen = gen_files_path('Module16', os.path.abspath(os.path.join('..', '..', '..')))

for i_file in file_gen:
    print(i_file)
