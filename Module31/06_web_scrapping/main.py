import requests
import re


def subtitles(link):
    my_request = requests.get(link)

    # with open('site.txt', 'w') as file:
    #     file.write(my_request.text)

    # with open('site.txt', 'r') as file:
    #     lst = list()
    #     for line in file:
    #         if line.startswith('<h3'):
    #             tag = re.findall(r'\>(.*)\<', line)
    #             lst.append(tag)
    #
    # subtitles = [i for sublst in lst for i in sublst]

    text = my_request.text
    subtitles = re.findall(r'<h3 .*?>(.*?)</h3>', text)
    return subtitles


print(subtitles('http://www.columbia.edu/~fdc/sample.html'))
