import itertools

# num = '0123456789'
# num = [j for i in range(4) for j in range(10)]
num = [i for i in range(10)]
lst = list()
# for item in itertools.combinations_with_replacement(num, 4):
for item in itertools.product(num, repeat=4):
    print(item)
    lst.append(item)
print('\nКоличество перестановок:', len(lst))




