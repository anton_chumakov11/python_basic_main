import re


data = 'А578ВЕ777 ОР233787 К901МН666 СТ46599 СНИ2929П777 666АМР666'

pattern_privat_car = r'[АВЕКМНОРСТУХ]\d{3}[АВЕКМНОРСТУХ]{2}\d{2,3}'
res = re.findall(pattern_privat_car, data)
print('Список номеров частных автомобилей:', res)

pattern_taxi = r'[АВЕКМНОРСТУХ]{2}\d{3}\d{2,3}'
res1 = re.findall(pattern_taxi, data)
print('Список номеров частных автомобилей:', res1)
