import re


lst = ['9999999999', '999999-999', '99999x9999', '889888899999999678678', '77780123456789666', '6669999999999']

for i_number in lst:
    print(lst.index(i_number) + 1, 'номер: ', end='')
    pattern = r'\b[89]\d{9}\b'
    res = re.findall(pattern, i_number)
    if len(res) == 1:
        print('всё в порядке')
    else:
        print('не подходит')
