import requests
import json


request_list_of_death = requests.get('https://www.breakingbadapi.com/api/deaths')
data_death = json.loads(request_list_of_death.text)

death_by_episode = dict()
for i_death in data_death:
    key = i_death['season'], i_death['episode']
    if key in death_by_episode:
        death_by_episode[key][0] += i_death['number_of_deaths']
        death_by_episode[key][1].append(i_death['death'])
    else:
        death_by_episode[key] = list()
        death_by_episode[key].append(i_death['number_of_deaths'])
        death_by_episode[key].append(list())
        death_by_episode[key][1].append(i_death['death'])

death_by_episode_true_value = max(death_by_episode.values())
death = death_by_episode_true_value[1]
max_count_of_dead = death_by_episode_true_value[0]


for key, value in death_by_episode.items():
    if value == death_by_episode_true_value:
        max_count_of_dead_key = key

max_count_of_dead_season = max_count_of_dead_key[0]
max_count_of_dead_episode = max_count_of_dead_key[1]

request_list_of_episodes = requests.get('https://www.breakingbadapi.com/api/episodes')
data_episodes = json.loads(request_list_of_episodes.text)

episode_max_death = dict()
for i_episod in data_episodes:
    if int(i_episod['season']) == max_count_of_dead_season and int(i_episod['episode']) == max_count_of_dead_episode:
        episode_max_death = i_episod

result = dict()
result['episode_id'] = episode_max_death['episode_id']
result['season'] = episode_max_death['season']
result['count_of_dead'] = max_count_of_dead
result['characters'] = death

with open('breaking_bad.json', 'w') as file:
    json.dump(result, file, indent=4)
