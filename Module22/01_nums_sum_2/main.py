incoming_file = open('numbers.txt', 'r',
                     encoding='utf-8')
data = incoming_file.read().replace(' ','').replace('\n', '')
incoming_file.close()
summ = 0
for i_letter in data:
    summ += int(i_letter)
result_file = open('answer.txt', 'w',
                   encoding='utf-8')
result_file.write(str(summ))
result_file.close()
