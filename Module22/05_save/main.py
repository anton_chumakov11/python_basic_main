import os

def input_path():
    path_list = input('Куда хотите сохранить документ? Введите последовательность папок (через пробел):\n').split(' ')
    path_list.insert(0, os.path.sep)
    pat = os.path.abspath(os.path.join(*path_list))
    while not os.path.exists(pat):
        print('Данной директории не существует.\nПроверьте путь и заново введите последовательность папок (через пробел):')
        path_list = input().split(' ')
        path_list.insert(0, os.path.sep)
        pat = os.path.abspath(os.path.join(*path_list))
    return pat

def save_file(path_to_file, string):
    if os.path.exists(path_to_file):
        answer = input('Вы действительно хотите перезаписать файл? ').lower()
        if answer == 'да':
            result_file = open(path_to_file, 'w', encoding='utf-8')
            result_file.write(string)
            result_file.close()
            print('Файл успешно сохранён!')
        else:
            print('Изменения не сохранены')
    else:
        result_file = open(path_to_file, 'w', encoding='utf-8')
        result_file.write(string)
        result_file.close()
        print('Файл успешно сохранён!')



text = input('Введите строку: ')
path = input_path()
file_name = input('Введите имя файла: ')
file_name += '.txt'
path_to_file = os.path.join(path, file_name)
save_file(path_to_file, text)



