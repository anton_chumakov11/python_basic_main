def cezar_k(string, k):
    alphabit_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    alphabit = list(alphabit_string)
    alphabit_string_low = alphabit_string.lower()
    alphabit_low = list(alphabit_string_low)
    lst_string = list(string)
    if k > len(alphabit):
         k %= len(alphabit)

    for i in range(len(lst_string)):
        if lst_string[i] in alphabit:
            index = alphabit.index(string[i])
            if index + k < len(alphabit):
                index += k
                lst_string[i] = alphabit[index]
            else:
                index = index - len(alphabit) + k
                lst_string[i] = alphabit[index]
        elif lst_string[i] in alphabit_low:
            index = alphabit_low.index(string[i])
            if index + k < len(alphabit_low):
                index += k
                lst_string[i] = alphabit_low[index]
            else:
                index = index - len(alphabit_low) + k
                lst_string[i] = alphabit_low[index]
    new_string = ''.join(lst_string)
    return new_string


def encription(file):
    k = 1
    for i_line in file:
        new_line = cezar_k(i_line, k)
        k += 1
        result_file = open('cipher_text.txt', 'a', encoding='utf-8')
        result_file.write(new_line)
    result_file.close()

incoming_file = open('text.txt', 'r', encoding='utf-8')
encription(incoming_file)
incoming_file.close()
