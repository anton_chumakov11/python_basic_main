import os

def file_and_dir_counter(path):
    file_counter = 0
    dir_counter = 0
    size = 0
    for i_elem in os.listdir(path):
        new_path = os.path.join(path, i_elem)
        if os.path.isdir(new_path):
            dir_counter += 1
            dir_counter1, file_counter1, size1 = file_and_dir_counter(new_path)
            dir_counter += dir_counter1
            file_counter += file_counter1
            size += size1
        elif os.path.isfile(new_path):
            file_counter += 1
            size += os.path.getsize(new_path)
    return dir_counter, file_counter, size

path = os.path.join('..', '..', 'Module14')

dirs, files, size = file_and_dir_counter(path)
size /= 1024
print('Размер каталога (в Кб): {s}\nКоличество подкаталогов: {d}\nКоличество файлов: {f}'
      .format(
    d=dirs,
    f=files,
    s=size
))
