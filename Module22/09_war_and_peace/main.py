import zipfile

def how_mach_letter(string):
    alphabit = 'bcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZабвгдеёжзийклмнопрстуфхцчшщьыъэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ'
    counter = 0
    for i_letter in string:
        if i_letter in alphabit:
            counter += 1
    return counter

def letters_count(string):
    alphabit = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZабвгдеёжзийклмнопрстуфхцчшщьыъэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ'
    letter_lst = list()
    tmp_list = list()
    for i_letter in string:
        if i_letter not in tmp_list and i_letter in alphabit:
            letter_lst.append([i_letter, string.count(i_letter)])
        tmp_list.append(i_letter)
    return letter_lst

zip = zipfile.ZipFile('voyna-i-mir.zip', 'r')
zip.extract('voyna-i-mir.txt')

file = open('voyna-i-mir.txt', 'r', encoding='utf-8')
text = file.read()
file.close()

lst = letters_count(text)
counter = how_mach_letter(text)
for i_elem in lst:
    i_elem[1] = round(i_elem[1] / counter, 5)

lst = sorted(lst, key=lambda x: -x[1])

outgoing_file = open('analysis.txt', 'a', encoding='utf-8')
for i_elem in lst:
    outgoing_file.write(str(i_elem[0]) + ': ' + str(i_elem[1]) + ', ')
outgoing_file.close()

# import collections
# import zipfile
# def unzip(archive):
#     zfile = zipfile.ZipFile(archive, 'r')
#     for i_file_name in zfile.namelist():
#         zfile.extract(i_file_name)
#     zfile.close()
# def collect_stats(file_name):
#     result = {}
#     if file_name.endswith('.zip'):
#         unzip(file_name)
#         file_name = ''.join((file_name[:-3], 'txt'))
#     text_file = open(file_name, 'r', encoding='utf-8')
#     for i_line in text_file:
#         for j_char in i_line:
#             if j_char.isalpha():
#                 if j_char not in result:
#                     result[j_char] = 0
#                 result[j_char] += 1
#     text_file.close()
#     return result
# def print_stats(stats):
#     print("+{:-^19}+".format('+'))
#     print("|{: ^9}|{: ^9}|".format('буква', 'частота'))
#     print("+{:-^19}+".format('+'))
#     for char, count in stats.items():
#         print("|{: ^9}|{: ^9}|".format(char, count))
#     print("+{:-^19}+".format('+'))
# def sort_by_frequency(stats_dict):
#     sorted_values = sorted(stats_dict.values(), reverse = True)
#     sorted_dict = collections.OrderedDict()
#     for i_value in sorted_values:
#         for j_key in stats_dict.keys():
#             if stats_dict[j_key] == i_value:
#                 sorted_dict[j_key] = stats_dict[j_key]
#     return sorted_dict
# file_name = 'voyna-i-mir.zip'
# stats = collect_stats(file_name)
# stats = sort_by_frequency(stats)
# print_stats(stats)

