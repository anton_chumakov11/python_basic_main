def how_mach_letter(string):
    alphabit = 'abcdefghijklmnopqrstuvwxyz'
    counter = 0
    for i_letter in string:
        if i_letter in alphabit:
            counter += 1
    return counter

def letters_count(string):
    alphabit = 'abcdefghijklmnopqrstuvwxyz'
    letter_lst = list()
    tmp_list = list()
    for i_letter in string:
        if i_letter not in tmp_list and i_letter in alphabit:
            letter_lst.append([i_letter, string.count(i_letter)])
        tmp_list.append(i_letter)
    return letter_lst

def sorting(lst):
    lst.sort(key = lambda x: x[1])
    print(lst)
    dct = dict()
    dct[lst[0][1]] = lst[0]
    for i_index in range(1, len(lst)):
        if lst[i_index][1] == lst[i_index - 1][1]:
            dct[lst[i_index][1]].append(lst[i_index])
    return dct

incoming_file = open('text.txt', 'r', encoding='utf-8')
text = incoming_file.read().lower()
lst = letters_count(text)
counter = how_mach_letter(text)
for i_elem in lst:
    i_elem[1] = round(i_elem[1] / counter, 3)

lst = sorted(lst, key=lambda x: (-x[1], x[0]))


outgoing_file = open('analysis.txt', 'a', encoding='utf-8')

for i_index in range(len(lst)):
    outgoing_file.write(str(lst[i_index][0]) + ' ' + str(lst[i_index][1]) + '\n')

outgoing_file.close()


