import os

def line_counter(file):
    counter = 0
    for i_line in file:
        counter += 1
    return counter

def letter_counter(file):
    data = file.read().replace('\n', '')
    extra_characters = " 1234567890-_=+!@#$%^&*()<>?,./\"'}{[]:;"
    for i_letter in extra_characters:
        data = data.replace(i_letter, '')
    counter = len(data)
    return counter

def word_counter(file):
    counter = 0
    for i_line in file:
        words_in_line = len(i_line.split(' '))
        counter += words_in_line
    return counter

path = os.path.join('..', '02_zen_of_python', 'zen.txt')

incoming_file = open(path, 'r', encoding='utf-8')
lines = line_counter(incoming_file)
print('Количество строк:', lines)
incoming_file.close()

incoming_file2 = open(path, 'r', encoding='utf-8')
letters = letter_counter(incoming_file2)
print('Количество букв:', letters)
incoming_file2.close()

incoming_file3 = open(path, 'r', encoding='utf-8')
words = word_counter(incoming_file3)
print('Количество слов:', words)
incoming_file3.close()
