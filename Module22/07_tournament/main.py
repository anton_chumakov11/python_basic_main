incoming_file = open('first_tour.txt', 'r', encoding='utf-8')
member_list = list()
for i_line in incoming_file:
    if i_line[0].isdigit():
        k = int(i_line.replace('\n', ''))
    else:
        member_list.append(i_line.replace('\n', '').split(' '))
incoming_file.close()

outgoing_file = open('second_tour.txt', 'a', encoding='utf-8')

for i_elem in member_list:
    if int(i_elem[2]) <= k:
        member_list.remove(i_elem)

member_list.sort(key = lambda x: x[2])

n = 1

outgoing_file.write(str(len(member_list)) + '\n')

for i_index in range(len(member_list) - 1, -1, -1):
    outgoing_file.write(str(n) + ') ' + member_list[i_index][1][0] + '. ' + member_list[i_index][0] + ' ' + member_list[i_index][2] + '\n')
    n += 1

outgoing_file.close()


