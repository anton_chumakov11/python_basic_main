import time


def delay(func):
    def wrapper (*args, **kwargs):
        time1 = time.time()
        time2 = time.time()
        print('Задержка в 5 секунд...')
        while time2 - time1 < 5:
            time2 = time.time()
        result = func(*args, **kwargs)
        return result
    return wrapper


@delay
def summa_n(n):
  summa = (n + 1) * n / 2
  print('Я знаю, что сумма чисел от 1 до', n, 'равна', summa)

summa_n(10)
