import functools


def counter(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        wrapper.counter += 1
        result = func(*args, **kwargs)
        print('{func} вызвана {counter} раз'.format(func=func.__name__, counter=wrapper.counter))
        return result
    wrapper.counter = 0
    return wrapper


@counter
def summa_n(n):
  summa = (n + 1) * n / 2
  print('Я знаю, что сумма чисел от 1 до', n, 'равна', summa)

summa_n(5)
summa_n(10)
summa_n(15)
