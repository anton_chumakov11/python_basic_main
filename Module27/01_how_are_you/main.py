def how_are_you(func):
    def wrapper (*args, **kwargs):
        print('\nКак дела? Хорошо. \nА у меня не очень! Ладно, держи свою функцию.')
        result = func(*args, **kwargs)
        return result
    return wrapper


@how_are_you
def test():
    print('<Тут что-то происходит...>')


@how_are_you
def summa_n(n):
  summa = (n + 1) * n / 2
  print('Я знаю, что сумма чисел от 1 до', n, 'равна', summa)


test()
summa_n(10)
