import functools
import time


def logging(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        error_file = open('function_errors.log', 'a', encoding='utf-8')
        print(func.__name__)
        print(func.__doc__)
        try:
            result = func(*args, **kwargs)
            return result
        except ZeroDivisionError:
            lst = [func.__name__, ': ZeroDivisionError', time.ctime()]
            error_file.write(' '.join(str(i_elem) for i_elem in lst) + '\n')
        error_file.close()
    return wrapper


@logging
def dis(a: int, b: int):
    """
    Функция, считающая отношения между двумя переданными числами
    :param a: (int) - первое переданное число
    :param b: (int) - второе переданное число
    :return: dis1 - отношение первого числа ко второму, dis2 - отношение второго числа к первому
    """
    dis1 = a / b
    dis2 = b / a
    return dis1, dis2


print(dis(0, 0))
