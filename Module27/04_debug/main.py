import functools


def debug(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        string1 = 'Вызывается ' + str(func.__name__) + "("
        string2 = ', '.join("'{}'".format(i_elem) for i_elem in args)
        if args and kwargs:
            string3 = ', '
        else:
            string3 = ''
        string4 = ', '.join('{}={}'.format(key, value) for key, value in kwargs.items())
        string5 = ')'
        string6 = string1 + string2 + string3 + string4 + string5
        print(string6)
        result = func(*args, **kwargs)
        print(str(func.__name__) + "вернула значение '" + result + "'")
        print(result)
        print()
    return wrapper


@debug
def greeting(name, age=None):
    if age:
        return "Ого, {name}! Тебе уже {age} лет, ты быстро растёшь!".format(name=name, age=age)
    else:
        return "Привет, {name}!".format(name=name)


greeting("Том")
greeting("Миша", age=100)
greeting(name="Катя", age=16)
