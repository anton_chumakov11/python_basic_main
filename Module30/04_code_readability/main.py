# Однострочный способ:
print(list(filter(lambda x: x != 2, filter(lambda x: len([i for i in range(2, x) if (x % i == 0)]) == 0, range(3, 1001)))))


# Обычный способ:
def is_simple(num):
    k = 0
    for i in range(2, num):
        if num % i == 0:
            k += 1
    if k <= 0:
        return True
    else:
        return False


def simple_list(max_num):
    simple_list = list()
    for i in range(3, max_num + 1):
        if is_simple(i):
            simple_list.append(i)
    if 2 in simple_list:
        simple_list.remove(2)
    return simple_list


print(simple_list(1001))


