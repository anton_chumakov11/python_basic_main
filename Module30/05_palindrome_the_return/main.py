import collections


def can_be_poly(str):
    count = collections.Counter(str)
    res = list(filter(lambda x: x % 2 != 0, count.values()))
    return len(res) <= 1


print(can_be_poly('ababc'))
print(can_be_poly('abbbc'))
