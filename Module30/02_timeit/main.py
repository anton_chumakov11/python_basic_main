import timeit


res: float = timeit.timeit('"-".join(str(n) for n in range(100))', number=10000)

print('Generator result', round(res, 5))


res1: float = timeit.timeit('"-".join([str(n) for n in range(100)])', number=10000)

print('Comprehansion result', round(res1, 5))


res2: float = timeit.timeit('"-".join(list(map(lambda x: str(x), range(100))))', number=10000)

print('map result', round(res2, 5))


