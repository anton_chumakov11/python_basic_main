def new_structure(dict):
    new_data = list()
    for i_key, i_value in dict.items():
        new_data.append(i_key + i_value)
    return new_data


players = {
    ("Ivan", "Volkin"): (10, 5, 13),
    ("Bob", "Robbin"): (7, 5, 14),
    ("Rob", "Bobbin"): (12, 8, 2)
}

new_players = new_structure(players)
print(new_players)


