def get_new_list(my_list):
    return list(zip(
        [i_value for i_index, i_value in enumerate(my_list) if i_index % 2 == 0],
        [i_value for i_index, i_value in enumerate(my_list) if i_index % 2 != 0]
    ))

def get_new_list1(my_list):
    new_list = list()
    for i_index in range(0, len(my_list), 2):
        new_list.append((my_list[i_index], my_list[i_index + 1]))
    return new_list

my_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

print('Оригинальный список:', my_list)
new_list = get_new_list(my_list)
print('Новый список:', new_list)
new_list1 = get_new_list1(my_list)
print('Новый список:', new_list)
