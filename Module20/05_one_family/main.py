def male_family(string):
    if string[-1::] == 'а':
        string = string[:-1:]
    else:
        string = string + 'а'
    return string

def print_persons(family, data):
    for i_person in data:
        if male_family(family) in i_person or family in i_person:
            print(i_person[0], i_person[1], data[i_person])

families_members = {
    ('Сидоров', 'Никита'): 35,
    ('Сидорова', 'Алина'): 34,
    ('Сидоров', 'Павел'): 10,
    ('Петров', 'Петр'): 54,
    ('Петрова', 'Снежанна'): 48,
    ('Петров', 'Сергей'): 26
}

family = input('Введите фамилию: ').capitalize()

print_persons(family, families_members)
