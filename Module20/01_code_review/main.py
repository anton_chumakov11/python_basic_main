def function(dict):
    set_interest = set()
    sername_length = 0
    for i_student in students.values():
        set_stud_interest = set(i_student['interests'])
        set_interest.update(set_stud_interest)
        sername_length += len(i_student['surname'])
    return list(set_interest), sername_length

students = {
    1: {
        'name': 'Bob',
        'surname': 'Vazovski',
        'age': 23,
        'interests': ['biology, swimming']
    },
    2: {
        'name': 'Rob',
        'surname': 'Stepanov',
        'age': 24,
        'interests': ['math', 'computer games', 'running']
    },
    3: {
        'name': 'Alexander',
        'surname': 'Krug',
        'age': 22,
        'interests': ['languages', 'health food']
    }
}

# def f(dict):
#     lst = []
#     string = ''
#     for i in dict:
#         lst += (dict[i]['interests'])
#         string += dict[i]['surname']
#     cnt = 0
#     for s in string:
#         cnt += 1
#     return lst, cnt

# pairs = []
# for i in students:
#     pairs += (i, students[i]['age'])

id_age = list()
for i_id, i_student in students.items():
    id_age.append((i_id, i_student['age']))
print(id_age)

# my_lst = f(students)[0]
# l = f(students)[1]
# print(my_lst, l)
interest_list, sername_length = function(students)
print('полный список интересов всех студентов:\n{interest_list}'
      f'\nобщая длину всех фамилий студентов:\n{sername_length}'.format(
    interest_list=interest_list,
    sername_length=sername_length
))

# TODO исправить код
