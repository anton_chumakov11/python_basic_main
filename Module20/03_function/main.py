def elem_indexes(list, elem):
    elem_indexes = []
    for i_index, i_thing in enumerate(list):
        if i_thing == elem:
            elem_indexes.append(i_index)
    return elem_indexes

def slice_tuple(my_tuple, element):
    new_tuple = list(my_tuple)
    element_indexes = elem_indexes(new_tuple, element)
    if len(element_indexes) >= 2:
        new_tuple = new_tuple[element_indexes[0]:(element_indexes[1] + 1)]
    elif len(element_indexes) == 1:
        new_tuple = new_tuple[element_indexes[0]:]
    else:
        new_tuple = []
    return tuple(new_tuple)

my_tuple = 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5
element = int(input('Введите элемент: '))

new_tuple = slice_tuple(my_tuple,element)
print(new_tuple)

