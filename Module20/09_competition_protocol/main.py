def check_record():
    record = input().split()
    while len(record) > 2 or not record[0].isdigit():
            print('Запись должна включать результат (цлое число) и имя участника (одно слово)!')
            record = input('Введите запись ещё раз: ').split()
    return record

def filling_protocol(num):
    protocol = dict()
    set_of_players = set()
    for i_record in range(1, num + 1):
        print(i_record, 'запись:', end = ' ')
        record = check_record()
        if record[1] in protocol:
            protocol[record[1]].append((i_record, int(record[0])))
            set_of_players.add(record[1])
        else:
            protocol[record[1]] = list()
            # temp_tuple = i_record, int(record[0])
            protocol[record[1]].append((i_record, int(record[0])))
            set_of_players.add(record[1])
    return protocol, set_of_players

def best_player_result(player, protocol):
    best_score = 0
    for data in protocol[player]:
        if data[1] > best_score:
            best_result = data
            best_score = data[1]
    return best_result

def best_players_results(protocol):
    best_results = dict()
    for player in protocol:
        best_results[player] = best_player_result(player, protocol)
    return best_results

def winer(best_dict):
    best_score = 0
    best_data = 0, 0
    for i_data in best_dict.values():
        if i_data[1] > best_score:
            best_data = i_data
            best_score = i_data[1]
        elif i_data[1] == best_data:
            if i_data[0] > best_data[0]:
                best_data = i_data
                best_score = i_data[1]
    for i_player, i_data in best_dict.items():
        if i_data == best_data:
            return i_player, i_data[1]

def show_result(best_dict):
    print('\nИтоги соревнований:')
    for place in range(1, 4):
        player, score = winer(best_dict)
        print(place, f'место. {player}({score})')
        best_dict.pop(player)


def game():
    number_of_records = int(input('Сколько записей вносится в протокол? '))
    while number_of_records < 3:
        number_of_records = int(input('В соревнованиях не менее трёх участников.\nВ протоколе не может быть менее трёх записей.\nСколько записей вносится в протокол? '))
    protocol, set_of_players = filling_protocol(number_of_records)
    while len(set_of_players) < 3:
        print('В соревнованиях должно быть не менее трёх участников. Повторите ввод записей')
        protocol, set_of_players = filling_protocol(number_of_records)
    # print(protocol)
    best_players_results_dict = best_players_results(protocol)
    # print(best_players_results_dict)
    show_result(best_players_results_dict)


game()



