def is_prime(num):
    for divider in range(2, num):
        if num % divider == 0:
            return False
    if num == 2:
        return False
    else:
        return True

def what_type(variable):
    new_var = None
    if isinstance(variable, (dict, list, str, tuple)):
        new_var = elements(variable)
    else:
        print('Не распознанный тип')
        new_ver = None
    return new_var

def elements(variable):
    return [i_elem for i_index, i_elem in enumerate(variable) if is_prime(i_index)]

my_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
my_dict = {0: 0, 1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9}
my_tuple = 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
string = '0123456789'
int = 999

print(what_type(my_list))
print(what_type(my_dict))
print(what_type(my_tuple))
print(what_type(string))
print(what_type(int))

