def isAllElemIsInt(my_tuple):
    for elem in my_tuple:
        if not isinstance(elem, int):
            return False
    return True

def sort_tuple(my_tuple):
    if isAllElemIsInt(my_tuple):
        return tuple(sorted(my_tuple))
    else:
        return my_tuple

my_tuple = 8, 12, 1, 34, 57, 4, 7
my_tuple = sort_tuple(my_tuple)
print(my_tuple)
my_tuple1 = 8, 12, 1, 34.5, 57, 4, 7
my_tuple1 = sort_tuple(my_tuple1)
print(my_tuple1)
