def my_zip(elem1, elem2):
    minlen = len(elem1)
    if len(elem2) < minlen:
        minlen = len(elem2)
    return ((elem1[i], elem2[i]) for i in range(minlen))

string = 'abcd'
my_tuple = 10, 20, 30, 40

generator = my_zip(string, my_tuple)
print(generator)


